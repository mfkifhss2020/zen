# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     realtimePush.py
# Description:  通知消息实时推送接口
# Author:       'zhouhanlin'
# CreateDate:   2020/03/02
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from datetime import date
from flask import current_app, request, session
from flask_restful import Resource, fields, reqparse, marshal

from webapp import db
from webapp.common.http.decorators import login_required
from webapp.common.http.respBody import RespBody, MsgDesc
from webapp.noticeManager.models.noticeInfo import NoticeInfo

query_fields = {
    'id': fields.Integer,
    'notice_title': fields.String,
    'notice_content': fields.String,
    'create_time': fields.String
}


class RealtimePushApi(Resource):

    # decorators = [check_http_headers, check_request_frequency]
    def __init__(self):
        self.__env = request.environ
        self.remote_ip = self.__env.get("REMOTE_ADDR")
        self.remote_port = self.__env.get("REMOTE_PORT")
        self.protocol = self.__env.get("SERVER_PROTOCOL").split("/")[0].lower()
        self.http_host = self.__env.get("HTTP_HOST")
        self.method = self.__env.get("REQUEST_METHOD")
        self.uri = self.__env.get("PATH_INFO")
        self.access_user = request.remote_user
        self.logger = current_app.logger

        if self.access_user:
            self.logger_formatter = "来自IP：<{}>".format(self.remote_ip) + \
                                    "的端口：[{}]的 {} 用户".format(self.remote_port, self.access_user) + \
                                    "通过 {} ".format(self.method) + \
                                    "方法访问url：{}".format(self.protocol) + "//" + \
                                    "{}".format(self.http_host) + \
                                    "{}".format(self.uri)
        else:
            self.logger_formatter = "来自IP：<{}>".format(self.remote_ip) + \
                                    "的端口：[{}]".format(self.remote_port) + \
                                    "通过 {} ".format(self.method) + \
                                    "方法访问url：{}".format(self.protocol) + "//" + \
                                    "{}".format(self.http_host) + \
                                    "{}".format(self.uri)

    @login_required
    def get(self):
        """
        通知实时推送接口
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)
        parse.add_argument("current_page", type=int, help="当前页数校验错误", required=True, location='args')
        parse.add_argument("per_page", type=int,
                           help="必须设置页码条目数为10，20，50，或者100",
                           choices=[10, 20, 50, 100],
                           required=True,
                           location='args'
                           )

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        # 降序, 必须要在生效时间内
        current_date = date.today()
        current_page = args.get("current_page")
        per_page = args.get("per_page")
        u_id = session.get("distributor_id")
        record = NoticeInfo.query.filter(
            NoticeInfo.start_time <= current_date, NoticeInfo.end_time >= current_date).order_by(
            NoticeInfo.create_time.desc(), NoticeInfo.status == "已推送").all()

        total = len(record)
        if total > 0:
            if total % per_page == 0:
                pages = total // per_page
            else:
                pages = total // per_page + 1
            if current_page > pages:
                self.logger.error(MsgDesc.h_401_100.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_401_100.value), 401
            offset_start = (current_page - 1) * per_page
            offset_end = current_page * per_page
            for i in record[offset_start:offset_end]:
                if i.read_user_id is None or i.read_user_id == "":
                    i.read_user_id = u_id
                    db.session.commit()
                else:
                    if i.read_user_id.find(u_id) == -1:
                        i.read_user_id = i.read_user_id + "|" + u_id
                        db.session.commit()
            self.logger.info(self.logger_formatter + " 成功...")
            return RespBody.custom(result="success", code=200,
                                   string=MsgDesc.h_200_200.value,
                                   data=marshal(record[offset_start:offset_end], query_fields),
                                   current_page=current_page,
                                   pages=pages,
                                   per_page=per_page,
                                   total=total), 200
        else:
            self.logger.error(MsgDesc.h_404_100.value)
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(string=MsgDesc.h_404_100.value), 404
