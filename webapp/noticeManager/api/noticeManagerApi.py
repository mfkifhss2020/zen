# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     noticeManagerApi.py
# Description:  通知管理API接口
# Author:       'zhouhanlin'
# CreateDate:   2020/03/01
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
import pymysql.err
import sqlalchemy.exc
from flask import current_app, request
from flask_restful import Resource, fields, reqparse, marshal

from webapp import db
from webapp.common.http.respBody import RespBody, MsgDesc
from webapp.noticeManager.models.noticeInfo import NoticeInfo
from webapp.common.http.decorators import login_required, access_auth

query_fields = {
    'id': fields.Integer,
    'notice_title': fields.String,
    'start_time': fields.String,
    'end_time': fields.String,
    'status': fields.String,
    'create_time': fields.String,
    'notice_content': fields.String
}


class NoticeManagerApi(Resource):

    # decorators = [check_http_headers, check_request_frequency]
    def __init__(self):
        self.__env = request.environ
        self.remote_ip = self.__env.get("REMOTE_ADDR")
        self.remote_port = self.__env.get("REMOTE_PORT")
        self.protocol = self.__env.get("SERVER_PROTOCOL").split("/")[0].lower()
        self.http_host = self.__env.get("HTTP_HOST")
        self.method = self.__env.get("REQUEST_METHOD")
        self.uri = self.__env.get("PATH_INFO")
        self.access_user = request.remote_user
        self.logger = current_app.logger

        if self.access_user:
            self.logger_formatter = "来自IP：<{}>".format(self.remote_ip) + \
                                    "的端口：[{}]的 {} 用户".format(self.remote_port, self.access_user) + \
                                    "通过 {} ".format(self.method) + \
                                    "方法访问url：{}".format(self.protocol) + "//" + \
                                    "{}".format(self.http_host) + \
                                    "{}".format(self.uri)
        else:
            self.logger_formatter = "来自IP：<{}>".format(self.remote_ip) + \
                                    "的端口：[{}]".format(self.remote_port) + \
                                    "通过 {} ".format(self.method) + \
                                    "方法访问url：{}".format(self.protocol) + "//" + \
                                    "{}".format(self.http_host) + \
                                    "{}".format(self.uri)

    @login_required
    @access_auth
    def get(self):
        """
        通知全量查询和模糊查询
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取args中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("current_page", type=int, help="当前页数校验错误", required=True, location='args')
        parse.add_argument("per_page", type=int,
                           help="必须设置页码条目数为10，20，50，或者100",
                           choices=[10, 20, 50, 100],
                           required=True,
                           location='args'
                           )

        parse.add_argument("keyword", type=str, required=False, location='args')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        keyword = args.get("keyword")
        current_page = args.get("current_page")
        per_page = args.get("per_page")
        if keyword is not None:
            filter_keyword = "%%" + keyword + "%%"
            record = NoticeInfo.query.filter(NoticeInfo.notice_title.like(filter_keyword)).order_by(
                NoticeInfo.create_time.desc()).all()

            total = len(record)
            if len(record) == 0:
                self.logger.error(MsgDesc.h_404_100.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_404_100.value), 404
            else:
                if total % per_page == 0:
                    pages = total // per_page
                else:
                    pages = total // per_page + 1
                # 如果请求的页面数大于数据的总页面数，将报错
                if current_page <= pages:
                    offset_start = (current_page - 1) * per_page
                    offset_end = current_page * per_page
                    self.logger.info(self.logger_formatter + " 成功...")
                    return RespBody.custom(result="success", code=200, string=MsgDesc.h_200_200.value,
                                           data=marshal(record[offset_start:offset_end], query_fields),
                                           current_page=current_page,
                                           per_page=per_page,
                                           pages=pages,
                                           total=total), 200
                else:
                    self.logger.error(MsgDesc.h_401_100.value)
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(string=MsgDesc.h_401_100.value), 401
        else:
            # 降序
            data = NoticeInfo.query.order_by(NoticeInfo.create_time.desc()).all()
            total = len(data)
            if total > 0:
                if total % per_page == 0:
                    pages = total // per_page
                else:
                    pages = total // per_page + 1
                if current_page > pages:
                    self.logger.error(MsgDesc.h_400_100.value)
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(string=MsgDesc.h_400_100.value), 400
                else:
                    offset_start = (current_page - 1) * per_page
                    offset_end = current_page * per_page
                    self.logger.info(self.logger_formatter + " 成功...")
                    return RespBody.custom(result="success", code=200, string=MsgDesc.h_200_200.value,
                                           data=marshal(data[offset_start:offset_end], query_fields),
                                           current_page=current_page,
                                           per_page=per_page,
                                           pages=pages,
                                           total=total), 200
            else:
                self.logger.error(MsgDesc.h_404_100.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_404_100.value), 404

    @login_required
    @access_auth
    def post(self):
        """
        新建通知的保存或者发布
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取form中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("action",
                           type=str,
                           choices=["保存", "发布"],
                           help='新建通知只能进行<保存>或者<发布>操作',
                           required=True,
                           location='form')
        parse.add_argument("notice_title", type=str, help='通知标题校验错误', required=True, location='form')
        parse.add_argument("start_time", type=str, help='通知生效时间校验错误', required=True, location='form')
        parse.add_argument("end_time", type=str, help='通知失效时间校验错误', required=True, location='form')
        parse.add_argument("content", type=str, help='通知内容校验错误', required=True, location='form')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)
        notice_title = args.get("notice_title")
        content = args.get("content")
        start_time = args.get("start_time")
        end_time = args.get("end_time")
        action = args.get("action")
        if len(notice_title) > 150 or len(content) > 1000:
            self.logger.error(MsgDesc.h_401_108.value)
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=108, string=MsgDesc.h_401_108.value), 401

        if action == "保存":
            notice_info = NoticeInfo(notice_title=notice_title,
                                     start_time=start_time,
                                     end_time=end_time,
                                     notice_content=content,
                                     status="待编辑",
                                     )
            db.session.add(notice_info)
        elif action == "发布":
            notice_info = NoticeInfo(notice_title=notice_title,
                                     start_time=start_time,
                                     end_time=end_time,
                                     notice_content=content,
                                     status="已推送",
                                     )
            db.session.add(notice_info)
        try:
            db.session.commit()
            db.session.remove()
            self.logger.info(self.logger_formatter + " 成功...")
            return RespBody.custom(result="success", code=200, string=MsgDesc.h_201_200.value), 201
        except (pymysql.err.IntegrityError, sqlalchemy.exc.IntegrityError) as e:
            db.session.rollback()
            db.session.remove()
            self.logger.error(MsgDesc.h_401_103.value + "<{}>".format(e))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=103, string=MsgDesc.h_401_103.value), 401
        except Exception as e:
            db.session.rollback()
            db.session.remove()
            self.logger.error(MsgDesc.h_411_100.value + "Reason: {}".format(e))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(string=MsgDesc.h_411_100.value), 411

    @login_required
    @access_auth
    def delete(self):
        """
        根据id删除通知
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取form中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("id", type=int, help='通知id校验错误', required=True, location='form')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        notice_id = args.get("id")
        record_1 = NoticeInfo.query.filter_by(id=notice_id).first()
        if record_1 is None:
            self.logger.error(MsgDesc.h_401_104.value + "<id: {}>".format(notice_id))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=104, string=MsgDesc.h_401_104.value), 401
        else:
            db.session.delete(record_1)
            try:
                db.session.commit()
                db.session.remove()
                self.logger.info(self.logger_formatter + " 成功...")
                return '', 204
            except Exception as e:
                db.session.rollback()
                db.session.remove()
                self.logger.error(MsgDesc.h_411_100.value + "Reason: {}".format(e))
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_411_100.value), 411

    @login_required
    @access_auth
    def put(self):
        """
        通知的编辑保存，发布和撤回
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取form中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("id", type=int, help='通知id校验错误', required=True, location='form')
        parse.add_argument("action",
                           type=str,
                           choices=["保存", "发布", "撤回"],
                           help='该通知只能进行<保存>，<发布>或者<撤回>操作',
                           required=True,
                           location='form')

        parse.add_argument("notice_title", type=str, help='通知标题校验错误', required=False, location='form')
        parse.add_argument("start_time", type=str, help='通知生效时间校验错误', required=False, location='form')
        parse.add_argument("end_time", type=str, help='通知失效时间校验错误', required=False, location='form')
        parse.add_argument("content", type=str, help='通知内容校验错误', required=False, location='form')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        notice_id = args.get("id")
        notice_title = args.get("notice_title")
        content = args.get("content")
        if notice_title is not None:
            if len(notice_title) > 150:
                self.logger.error(MsgDesc.h_401_108.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(code=108, string=MsgDesc.h_401_108.value), 401
        if content is not None:
            if len(content) > 1000:
                self.logger.error(MsgDesc.h_401_108.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(code=108, string=MsgDesc.h_401_108.value), 401
        record_1 = NoticeInfo.query.filter_by(id=notice_id).first()
        if record_1 is None:
            self.logger.error(MsgDesc.h_401_105.value + "<id: {}>".format(notice_id))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=105, string=MsgDesc.h_401_105.value), 401
        else:
            action = args.get("action")
            start_time = args.get("start_time")
            end_time = args.get("end_time")
            if action == "撤回":
                if record_1.status == "已推送":
                    record_1.status = "已撤回"
                # 非推送的通知不允许操作撤回
                else:
                    self.logger.error(MsgDesc.h_401_101.value + "<id: {}>".format(notice_id))
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(code=101, string=MsgDesc.h_401_101.value), 401
            elif action == '保存':
                # 发布的通知不允许编辑
                if record_1.status == "已推送":
                    self.logger.error(MsgDesc.h_401_102.value + "<id: {}>".format(notice_id))
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(code=102, string=MsgDesc.h_401_102.value), 401
                # 此处的保存是不会更新通知状态
                else:
                    if notice_title is not None:
                        record_1.notice_title = notice_title
                    if start_time is not None:
                        record_1.start_time = start_time
                    if end_time is not None:
                        record_1.end_time = end_time
                    if content is not None:
                        record_1.content = content
            elif action == '发布':
                if record_1.status == "已推送":
                    self.logger.error(MsgDesc.h_401_102.value + "<id: {}>".format(notice_id))
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(code=102, string=MsgDesc.h_401_102.value), 401
                # 此处的发布需要更新通知状态到已发布
                else:
                    if notice_title is not None:
                        record_1.notice_title = notice_title
                    if start_time is not None:
                        record_1.start_time = start_time
                    if end_time is not None:
                        record_1.end_time = end_time
                    if content is not None:
                        record_1.content = content
                    record_1.status = "已推送"
            try:
                db.session.commit()
                db.session.remove()
                self.logger.info(self.logger_formatter + " 成功...")
                return RespBody.custom(result="success", code=200, string=MsgDesc.h_201_200.value), 201
            except Exception as e:
                db.session.rollback()
                db.session.remove()
                self.logger.error(MsgDesc.h_411_100.value + "Reason: {}".format(e))
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_411_100.value), 411
