# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     billInfo.py
# Description:  单据信息模型
# Author:       'zhouhanlin'
# CreateDate:   2020/02/20
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from sqlalchemy import TIMESTAMP, text, Column, String, Integer

from webapp import db


class BillInfo(db.Model):
    """
    专卖店数据模型
    SQL建表语句：
                drop table if exists `t_bill_info`;
                create table `t_bill_info` (
                `id` int ( 11 ) not null auto_increment comment '主键',
                `order_id` varchar ( 64 ) not null unique comment '订单id由时间拼接而成，值唯一',
                `country` varchar ( 255 ) not null comment '国家',
                `distributor_id` varchar ( 255 ) not null comment '经销商id',
                `distributor_name` varchar ( 255 ) not null comment '经销商名称',
                `room_id` varchar ( 255 ) not null comment '店铺id',
                `contact` varchar ( 32 ) null comment '联系方式',
                `introducer_id` varchar ( 255 ) not null comment '介绍人id',
                `introducer_name` varchar ( 64 ) null comment '介绍人名称',
                `superior_id` varchar ( 255 ) not null comment '上级id',
                `superior_name` varchar ( 64 ) null comment '上级名称',
                `bank_name` varchar ( 255 ) not null comment '银行名称',
                `bank_account_id` varchar ( 64 ) null comment '银行账户id',
                `bank_account_name` varchar ( 255 ) null comment '银行账户名称',
                `full_or_half_bill` varchar ( 8 ) not null comment '全/半单',
                `order_status` varchar ( 64 ) default '未审核' not null comment '单据状态',
                `create_time` datetime DEFAULT CURRENT_TIMESTAMP comment '单据创建时间',
                `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '单据修改时间',
                primary key ( `id` )
                ) default charset = utf8 comment '单据信息表';
    """
    __tablename__ = "t_bill_info"
    id = Column("id", Integer, primary_key=True, autoincrement=True, comment="主键")
    order_id = Column("order_id", String(64), nullable=False, unique=True, comment="订单id由时间拼接而成，值唯一")
    country = Column("country", String(255), nullable=False, comment="国家")
    room_id = Column("room_id", String(255), nullable=False, comment="门店id")
    distributor_id = Column("distributor_id", String(255), nullable=False, unique=True, comment="经销商id，值唯一")
    distributor_name = Column("distributor_name", String(255), nullable=False, comment="经销商名字")
    certificate_id = Column("certificate_id", String(255), nullable=False, comment="证件号")
    contact = Column("contact", String(32), nullable=True, comment="联系方式")
    introducer_id = Column("introducer_id", String(255), nullable=False, comment="介绍人id")
    introducer_name = Column("introducer_name", String(64), nullable=True, comment="介绍人名称")
    superior_id = Column("superior_id", String(255), nullable=False, comment="上级id")
    superior_name = Column("superior_name", String(64), nullable=True, comment="上级名称")
    bank_name = Column("bank_name", String(255), nullable=True, comment="银行名称")
    bank_account_id = Column("bank_account_id", String(64), nullable=True, comment="银行账户id")
    bank_account_name = Column("bank_account_name", String(255), nullable=True, comment="银行账户名称")
    full_or_half_bill = Column("full_or_half_bill", String(8), nullable=False, comment="全/半单")
    order_status = Column("order_status", String(64), server_default="未审核", nullable=False, comment="单据状态")

    # 条目的创建时间
    create_time = Column("create_time",
                         TIMESTAMP(True),
                         server_default=text('CURRENT_TIMESTAMP'),
                         nullable=False,
                         comment="单据创建时间'"
                         )

    # 条目更新的时候，时间自动更新
    update_time = Column("update_time",
                         TIMESTAMP(True),
                         server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
                         nullable=False,
                         comment="单据信息修改时间"
                         )

    # 给表添加注释
    __table_args__ = ({'comment': '单据信息表'})

    # 模型对象转字典
    @staticmethod
    def to_dict(result):
        from collections import Iterable
        # 转换完成后，删除  '_sa_instance_state' 特殊属性
        try:
            if isinstance(result, Iterable):
                tmp = [dict(zip(res.__dict__.keys(), res.__dict__.values())) for res in result]
                for t in tmp:
                    t.pop('_sa_instance_state')
            else:
                tmp = dict(zip(result.__dict__.keys(), result.__dict__.values()))
                tmp.pop('_sa_instance_state')
            return tmp
        except BaseException:
            raise TypeError('Type error of parameter')
