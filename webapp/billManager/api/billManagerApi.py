# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     billManagerApi.py
# Description:  单据管理Api接口
# Author:       'zhouhanlin'
# CreateDate:   2020/02/20
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
import pymysql.err
import sqlalchemy.exc
from flask import current_app, request
from flask_restful import Resource, fields, reqparse, marshal

from webapp import db
from webapp.userManager.models.User import User
from webapp.billManager.models.billInfo import BillInfo
from webapp.bonusTotal.utils.bonusCalc import bonus_audit
from webapp.common.http.respBody import RespBody, MsgDesc
from webapp.common.utils.datetimeFormatter import str_to_dt
from webapp.common.utils.generateCode import get_order_code
from webapp.salesRoomManager.models.salesRoom import SalesRoom
from webapp.common.http.decorators import login_required, access_auth
from webapp.common.utils.paramCheck import is_isalnum, is_datet_str, is_datetime_str

query_fields = {
    'order_id': fields.String,
    'distributor_id': fields.String,
    'distributor_name': fields.String,
    'room_id': fields.String,
    'country': fields.String,
    'contact': fields.String,
    'certificate_id': fields.String,
    'introducer_id': fields.String,
    'introducer_name': fields.String,
    'superior_id': fields.String,
    'superior_name': fields.String,
    'bank_name': fields.String,
    'bank_account_id': fields.String,
    'bank_account_name': fields.String,
    'full_or_half_bill': fields.String,
    'order_status': fields.String,
    'create_time': fields.String
}


class BillInfoApi(Resource):

    # decorators = [check_http_headers, check_request_frequency]
    def __init__(self):
        self.__env = request.environ
        self.remote_ip = self.__env.get("REMOTE_ADDR")
        self.remote_port = self.__env.get("REMOTE_PORT")
        self.protocol = self.__env.get("SERVER_PROTOCOL").split("/")[0].lower()
        self.http_host = self.__env.get("HTTP_HOST")
        self.method = self.__env.get("REQUEST_METHOD")
        self.uri = self.__env.get("PATH_INFO")
        self.access_user = request.remote_user
        self.logger = current_app.logger

        if self.access_user:
            self.logger_formatter = "来自IP：<{}>".format(self.remote_ip) + \
                                    "的端口：[{}]的 {} 用户".format(self.remote_port, self.access_user) + \
                                    "通过 {} ".format(self.method) + \
                                    "方法访问url：{}".format(self.protocol) + "//" + \
                                    "{}".format(self.http_host) + \
                                    "{}".format(self.uri)
        else:
            self.logger_formatter = "来自IP：<{}>".format(self.remote_ip) + \
                                    "的端口：[{}]".format(self.remote_port) + \
                                    "通过 {} ".format(self.method) + \
                                    "方法访问url：{}".format(self.protocol) + "//" + \
                                    "{}".format(self.http_host) + \
                                    "{}".format(self.uri)

    @login_required
    @access_auth
    def get(self):
        """
        全量查询，时间段查询，模糊查询订单信息
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取args中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("current_page", type=int, help="当前页数校验错误", required=True, location='args')
        parse.add_argument("per_page", type=int,
                           help="必须设置页码条目数为10，20，50，或者100",
                           choices=[10, 20, 50, 100],
                           required=True,
                           location='args'
                           )

        parse.add_argument("keyword", type=str, required=False, location='args')
        parse.add_argument("start_time", type=str, required=False, location='args')
        parse.add_argument("end_time", type=str, required=False, location='args')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        keyword = args.get("keyword")
        start_time = args.get("start_time")
        end_time = args.get("end_time")
        current_page = args.get("current_page")
        per_page = args.get("per_page")

        # 模糊查询， 开始时间， 结束时间三个参数都有值
        if keyword is not None and start_time is not None and end_time is not None:
            filter_keyword = "%%" + keyword + "%%"
            if is_datet_str(start_time) or is_datetime_str(start_time):
                pass
            else:
                self.logger.error(MsgDesc.h_401_115.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(code=115, string=MsgDesc.h_401_115.value), 401
            if is_datet_str(end_time) or is_datetime_str(end_time):
                pass
            else:
                self.logger.error(MsgDesc.h_401_115.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(code=115, string=MsgDesc.h_401_115.value), 401
            start_time = str_to_dt(start_time)
            end_time = str_to_dt(end_time)
            record = BillInfo.query.filter(
                (BillInfo.order_id.like(filter_keyword) |
                 BillInfo.distributor_id.like(
                     filter_keyword) | BillInfo.distributor_name.like(
                            filter_keyword) | BillInfo.room_id.like(
                            filter_keyword)),
                BillInfo.create_time >= start_time,
                BillInfo.create_time <= end_time).order_by(
                BillInfo.create_time.desc()).all()
            total = len(record)
            if total == 0:
                self.logger.error(MsgDesc.h_404_100.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_404_100.value), 404
            else:
                if total % per_page == 0:
                    pages = total // per_page
                else:
                    pages = total // per_page + 1
                # 如果请求的页面数大于数据的总页面数，将报错
                if current_page <= pages:
                    offset_start = (current_page - 1) * per_page
                    offset_end = current_page * per_page
                    self.logger.info(self.logger_formatter + " 成功...")
                    return RespBody.custom(result="success",
                                           code=200,
                                           string=MsgDesc.h_200_200.value,
                                           data=marshal(record[offset_start:offset_end], query_fields),
                                           current_page=current_page,
                                           per_page=per_page,
                                           pages=pages,
                                           total=total), 200
                else:
                    self.logger.error(MsgDesc.h_400_100.value)
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(string=MsgDesc.h_400_100.value), 400

        # 模糊查询， 开始时间有值， 结束时间无值
        elif keyword is not None and start_time is not None and end_time is None:
            filter_keyword = "%%" + keyword + "%%"
            if is_datet_str(start_time) or is_datetime_str(start_time):
                pass
            else:
                self.logger.error(MsgDesc.h_401_115.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(code=115, string=MsgDesc.h_401_115.value), 401
            start_time = str_to_dt(start_time)
            record = BillInfo.query.filter(
                (BillInfo.order_id.like(filter_keyword) |
                 BillInfo.distributor_id.like(
                     filter_keyword) | BillInfo.distributor_name.like(
                            filter_keyword) | BillInfo.room_id.like(
                            filter_keyword)), BillInfo.create_time >= start_time).order_by(
                BillInfo.create_time.desc()).all()
            total = len(record)
            if total == 0:
                self.logger.error(MsgDesc.h_404_100.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_404_100.value), 404
            else:
                if total % per_page == 0:
                    pages = total // per_page
                else:
                    pages = total // per_page + 1
                # 如果请求的页面数大于数据的总页面数，将报错
                if current_page <= pages:
                    offset_start = (current_page - 1) * per_page
                    offset_end = current_page * per_page
                    self.logger.info(self.logger_formatter + " 成功...")
                    return RespBody.custom(result="success",
                                           code=200,
                                           string=MsgDesc.h_200_200.value,
                                           data=marshal(record[offset_start:offset_end], query_fields),
                                           current_page=current_page,
                                           per_page=per_page,
                                           pages=pages,
                                           total=total), 200
                else:
                    self.logger.error(MsgDesc.h_400_100.value)
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(string=MsgDesc.h_400_100.value), 400

        # 模糊查询， 结束时间有值， 开始时间无值
        elif keyword is not None and start_time is None and end_time is not None:
            filter_keyword = "%%" + keyword + "%%"
            if is_datet_str(end_time) or is_datetime_str(end_time):
                pass
            else:
                self.logger.error(MsgDesc.h_401_115.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(code=115, string=MsgDesc.h_401_115.value), 401
            end_time = str_to_dt(end_time)
            record = BillInfo.query.filter(
                (BillInfo.order_id.like(filter_keyword) |
                 BillInfo.distributor_id.like(
                     filter_keyword) | BillInfo.distributor_name.like(
                            filter_keyword) | BillInfo.room_id.like(
                            filter_keyword)), BillInfo.create_time <= end_time).order_by(
                BillInfo.create_time.desc()).all()

            total = len(record)
            if total == 0:
                self.logger.error(MsgDesc.h_404_100.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_404_100.value), 404
            else:
                if total % per_page == 0:
                    pages = total // per_page
                else:
                    pages = total // per_page + 1
                # 如果请求的页面数大于数据的总页面数，将报错
                if current_page <= pages:
                    offset_start = (current_page - 1) * per_page
                    offset_end = current_page * per_page
                    self.logger.info(self.logger_formatter + " 成功...")
                    return RespBody.custom(result="success",
                                           code=200,
                                           string=MsgDesc.h_200_200.value,
                                           data=marshal(record[offset_start:offset_end], query_fields),
                                           current_page=current_page,
                                           per_page=per_page,
                                           pages=pages,
                                           total=total), 200
                else:
                    self.logger.error(MsgDesc.h_400_100.value)
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(string=MsgDesc.h_400_100.value), 400

        # 开始时间， 结束时间有值， 模糊搜索无值
        elif keyword is None and start_time is not None and end_time is not None:
            if is_datet_str(start_time) or is_datetime_str(start_time):
                pass
            else:
                self.logger.error(MsgDesc.h_401_115.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(code=115, string=MsgDesc.h_401_115.value), 401
            if is_datet_str(end_time) or is_datetime_str(end_time):
                pass
            else:
                self.logger.error(MsgDesc.h_401_115.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(code=115, string=MsgDesc.h_401_115.value), 401
            start_time = str_to_dt(start_time)
            end_time = str_to_dt(end_time)
            record = BillInfo.query.filter(BillInfo.create_time >= start_time,
                                           BillInfo.create_time <= end_time).order_by(
                BillInfo.create_time.desc()).all()
            total = len(record)
            if total == 0:
                self.logger.error(MsgDesc.h_404_100.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_404_100.value), 404
            else:
                if total % per_page == 0:
                    pages = total // per_page
                else:
                    pages = total // per_page + 1
                # 如果请求的页面数大于数据的总页面数，将报错
                if current_page <= pages:
                    offset_start = (current_page - 1) * per_page
                    offset_end = current_page * per_page
                    self.logger.info(self.logger_formatter + " 成功...")
                    return RespBody.custom(result="success",
                                           code=200,
                                           string=MsgDesc.h_200_200.value,
                                           data=marshal(record[offset_start:offset_end], query_fields),
                                           current_page=current_page,
                                           per_page=per_page,
                                           pages=pages,
                                           total=total), 200
                else:
                    self.logger.error(MsgDesc.h_400_100.value)
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(string=MsgDesc.h_400_100.value), 400

        # 模糊查询有值， 开始时间， 结束时间无值
        elif keyword is not None and start_time is None and end_time is None:
            filter_keyword = "%%" + keyword + "%%"
            record = BillInfo.query.filter(BillInfo.order_id.like(filter_keyword) | BillInfo.distributor_id.like(
                filter_keyword) | BillInfo.distributor_name.like(filter_keyword) | BillInfo.room_id.like(
                filter_keyword)).order_by(BillInfo.create_time.desc()).all()

            total = len(record)
            if total == 0:
                self.logger.error(MsgDesc.h_404_100.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_404_100.value), 404
            else:
                if total % per_page == 0:
                    pages = total // per_page
                else:
                    pages = total // per_page + 1
                # 如果请求的页面数大于数据的总页面数，将报错
                if current_page <= pages:
                    offset_start = (current_page - 1) * per_page
                    offset_end = current_page * per_page
                    self.logger.info(self.logger_formatter + " 成功...")
                    return RespBody.custom(result="success",
                                           code=200,
                                           string=MsgDesc.h_200_200.value,
                                           data=marshal(record[offset_start:offset_end], query_fields),
                                           current_page=current_page,
                                           per_page=per_page,
                                           pages=pages,
                                           total=total), 200
                else:
                    self.logger.error(MsgDesc.h_400_100.value)
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(string=MsgDesc.h_400_100.value), 400

        # 结束时间有值， 开始时间， 模糊查询无值
        elif keyword is None and start_time is None and end_time is not None:
            if is_datet_str(end_time) or is_datetime_str(end_time):
                pass
            else:
                self.logger.error(MsgDesc.h_401_115.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(code=115, string=MsgDesc.h_401_115.value), 401
            end_time = str_to_dt(end_time)
            record = BillInfo.query.filter(BillInfo.create_time <= end_time).order_by(BillInfo.create_time.desc()).all()

            total = len(record)
            if total == 0:
                self.logger.error(MsgDesc.h_404_100.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_404_100.value), 404
            else:
                if total % per_page == 0:
                    pages = total // per_page
                else:
                    pages = total // per_page + 1
                # 如果请求的页面数大于数据的总页面数，将报错
                if current_page <= pages:
                    offset_start = (current_page - 1) * per_page
                    offset_end = current_page * per_page
                    self.logger.info(self.logger_formatter + " 成功...")
                    return RespBody.custom(result="success",
                                           code=200,
                                           string=MsgDesc.h_200_200.value,
                                           data=marshal(record[offset_start:offset_end], query_fields),
                                           current_page=current_page,
                                           per_page=per_page,
                                           pages=pages,
                                           total=total), 200
                else:
                    self.logger.error(MsgDesc.h_400_100.value)
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(string=MsgDesc.h_400_100.value), 400

        # 开始时间有值， 结束时间， 模糊查询无值
        elif keyword is None and start_time is not None and end_time is None:
            if is_datet_str(start_time) or is_datetime_str(start_time):
                pass
            else:
                self.logger.error(MsgDesc.h_401_115.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(code=115, string=MsgDesc.h_401_115.value), 401
            start_time = str_to_dt(start_time)
            record = BillInfo.query.filter(BillInfo.create_time >= start_time).order_by(
                BillInfo.create_time.desc()).all()

            total = len(record)
            if total == 0:
                self.logger.error(MsgDesc.h_404_100.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_404_100.value), 404
            else:
                if total % per_page == 0:
                    pages = total // per_page
                else:
                    pages = total // per_page + 1
                # 如果请求的页面数大于数据的总页面数，将报错
                if current_page <= pages:
                    offset_start = (current_page - 1) * per_page
                    offset_end = current_page * per_page
                    self.logger.info(self.logger_formatter + " 成功...")
                    return RespBody.custom(result="success",
                                           code=200,
                                           string=MsgDesc.h_200_200.value,
                                           data=marshal(record[offset_start:offset_end], query_fields),
                                           current_page=current_page,
                                           per_page=per_page,
                                           pages=pages,
                                           total=total), 200
                else:
                    self.logger.error(MsgDesc.h_400_100.value)
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(string=MsgDesc.h_400_100.value), 400

        # 模糊查询， 开始时间， 结束时间三个参数都无值
        else:
            data = BillInfo.query.order_by(BillInfo.create_time.desc()).all()
            total = len(data)
            if total == 0:
                self.logger.error(MsgDesc.h_404_100.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_404_100.value), 404
            else:
                if total % per_page == 0:
                    pages = total // per_page
                else:
                    pages = total // per_page + 1
                # 如果请求的页面数大于数据的总页面数，将报错
                if current_page <= pages:
                    offset_start = (current_page - 1) * per_page
                    offset_end = current_page * per_page
                    self.logger.info(self.logger_formatter + " 成功...")
                    return RespBody.custom(result="success",
                                           code=200,
                                           string=MsgDesc.h_200_200.value,
                                           data=marshal(data[offset_start:offset_end], query_fields),
                                           current_page=current_page,
                                           per_page=per_page,
                                           pages=pages,
                                           total=total), 200
                else:
                    self.logger.error(MsgDesc.h_400_100.value)
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(string=MsgDesc.h_400_100.value), 400

    @login_required
    @access_auth
    def post(self):
        """
        新增录单信息
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取form中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("country", type=str, help='国家验证错误', required=True, location='form')
        parse.add_argument("room_id", type=str, help='门店id验证错误', required=True, location='form')
        parse.add_argument("distributor_id", type=str, help='经销商id验证错误', required=True, location='form')
        parse.add_argument("distributor_name", type=str, help='经销商名称验证错误', required=True, location='form')
        parse.add_argument("certificate_id", type=str, help='证件号验证错误', required=True, location='form')
        parse.add_argument("full_or_half_bill", type=str, choices=["是", "否"], help='只能是否或是', required=True,
                           location='form'
                           )

        parse.add_argument("contact", type=str, help='联系方式验证错误', required=False, location='form')
        parse.add_argument("introducer_id", type=str, help='介绍人id验证错误', required=False, location='form')
        parse.add_argument("introducer_name", type=str, help='介绍人名称验证错误', required=False, location='form')
        parse.add_argument("superior_id", type=str, help='上级id验证错误', required=False, location='form')
        parse.add_argument("superior_name", type=str, help='上级名称验证错误', required=False, location='form')
        parse.add_argument("bank_name", type=str, help='银行名称验证错误', required=False, location='form')
        parse.add_argument("bank_account_id", type=str, help='银行账户id验证错误', required=False, location='form')
        parse.add_argument("bank_account_name", type=str, help='银行账户名称验证错误', required=False, location='form')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        distributor_id = args.get("distributor_id")
        # 判断distributor_id在SalesRoom表中是否存在
        record_9 = SalesRoom.query.filter_by(distributor_id=distributor_id).first()
        if record_9:
            self.logger.error(MsgDesc.h_401_103.value + "<{}>".format(distributor_id))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=103, string=MsgDesc.h_401_103.value), 401
        introducer_id = args.get("introducer_id")
        superior_id = args.get("superior_id")
        room_id = args.get("room_id")

        # distributor_id 必须是合法
        if not is_isalnum(distributor_id):
            self.logger.error(MsgDesc.h_401_100.value + "<{}> Only letters or numbers.".format(distributor_id))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(string=MsgDesc.h_401_100.value), 401

        # introducer_id 和 superior_id 必须要系统中都要存在
        record_1 = BillInfo.query.filter_by(distributor_id=introducer_id).first()
        record_2 = SalesRoom.query.filter_by(distributor_id=introducer_id).first()
        record_3 = BillInfo.query.filter_by(distributor_id=superior_id).first()
        record_4 = SalesRoom.query.filter_by(distributor_id=superior_id).first()
        record_5 = SalesRoom.query.filter_by(room_id=room_id).first()

        # 如果 introducer_id 在系统中不存在
        if record_1 is None and record_2 is None:
            self.logger.error(MsgDesc.h_401_116.value + "<{}>".format(introducer_id))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=116, string=MsgDesc.h_401_116.value), 401

        # 如果 superior_id 在系统中不存在
        if record_3 is None and record_4 is None:
            self.logger.error(MsgDesc.h_401_117.value + "<{}>".format(superior_id))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=117, string=MsgDesc.h_401_117.value), 401

        # room_id 在系统中必须存在
        if record_5 is None:
            self.logger.error(MsgDesc.h_401_118.value + "<{}>".format(room_id))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=118, string=MsgDesc.h_401_118.value), 401

        # 如果distributor_id在user中不存在，则需要将distributor_id插入user表
        record = User.query.filter_by(distributor_id=distributor_id).first()
        if record is None:
            # 生成用户的默认密码
            sub_str = distributor_id[-6:]
            user = User(distributor_id=distributor_id,
                        password=sub_str,
                        distributor_status='未审核')
            db.session.add(user)

        bill_info = BillInfo(order_id=get_order_code(),
                             room_id=room_id,
                             country=args.get("country"),
                             distributor_id=distributor_id,
                             distributor_name=args.get("distributor_name"),
                             certificate_id=args.get("certificate_id"),
                             contact=args.get("contact"),
                             introducer_id=introducer_id,
                             introducer_name=args.get("introducer_name"),
                             superior_id=superior_id,
                             superior_name=args.get("superior_name"),
                             bank_name=args.get("bank_name"),
                             bank_account_id=args.get("bank_account_id"),
                             bank_account_name=args.get("bank_account_name"),
                             full_or_half_bill=args.get("full_or_half_bill")
                             )
        db.session.add(bill_info)
        try:
            db.session.commit()
            db.session.remove()
            self.logger.info(self.logger_formatter + " 成功...")
            return RespBody.custom(result="success", code=200, string=MsgDesc.h_201_200.value), 201
        except (pymysql.err.IntegrityError, sqlalchemy.exc.IntegrityError) as e:
            db.session.rollback()
            db.session.remove()
            self.logger.error(MsgDesc.h_401_103.value + "<{}>".format(e))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=103, string=MsgDesc.h_401_103.value), 401
        except Exception as e:
            db.session.rollback()
            db.session.remove()
            self.logger.error(MsgDesc.h_411_100.value + "Reason: {}".format(e))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(string=MsgDesc.h_411_100.value), 411

    @login_required
    @access_auth
    def delete(self):
        """
        删除单条单据
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取form中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("order_id", type=str, help='订单id验证错误', required=True, location='form')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        record = BillInfo.query.filter_by(order_id=args.get("order_id")).first()
        if record is None:
            self.logger.error(MsgDesc.h_401_104.value + "<order_id: {}>".format(args.get("order_id")))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=104, string=MsgDesc.h_401_104.value), 401
        else:
            distributor_id = record.distributor_id
            db.session.delete(record)
            record_3 = False
            # 判断该经销商是否还有其他订单
            # 如果没有，则同时删除User表中的相关数据
            record_2 = BillInfo.query.filter_by(distributor_id=distributor_id).first()
            if record_2 is None:
                record_3 = User.query.filter_by(distributor_id=distributor_id).first()
                if record_3:
                    db.session.delete(record_3)
            try:
                db.session.commit()
                db.session.remove()
                if record_3:
                    bonus_audit()
                self.logger.info(self.logger_formatter + " 成功...")
                return '', 204
            except Exception as e:
                db.session.rollback()
                db.session.remove()
                self.logger.error(MsgDesc.h_411_100.value + "Reason: {}".format(e))
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_411_100.value), 411

    @login_required
    @access_auth
    def put(self):
        """
        修改录单信息，单据审核，反审核
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取form中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("order_id", type=str, help='订单id验证错误', required=True, location='form')
        parse.add_argument("country", type=str, help='国家验证错误', required=False, location='form')
        parse.add_argument("room_id", type=str, help='门店id验证错误', required=False, location='form')
        # parse.add_argument("distributor_id", type=str, help='经销商id验证错误', required=True, location='form')
        parse.add_argument("distributor_name", type=str, help='经销商名称验证错误', required=False, location='form')
        parse.add_argument("certificate_id", type=str, help='证件号验证错误', required=False, location='form')
        parse.add_argument("full_or_half_bill", type=str, choices=["是", "否"], help='只能是否或是', required=False,
                           location='form')
        parse.add_argument("order_status", type=str, choices=["未审核", "已审核"], help='只能是未审核或已审核',
                           required=False, location='form')

        parse.add_argument("contact", type=str, help='联系方式验证错误', required=False, location='form')
        parse.add_argument("introducer_id", type=str, help='介绍人id验证错误', required=False, location='form')
        parse.add_argument("introducer_name", type=str, help='介绍人名称验证错误', required=False, location='form')
        parse.add_argument("superior_id", type=str, help='上级id验证错误', required=False, location='form')
        parse.add_argument("superior_name", type=str, help='上级名称验证错误', required=False, location='form')
        parse.add_argument("bank_name", type=str, help='银行名称验证错误', required=False, location='form')
        parse.add_argument("bank_account_id", type=str, help='银行账户id验证错误', required=False, location='form')
        parse.add_argument("bank_account_name", type=str, help='银行账户名称验证错误', required=False, location='form')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        order_id = args.get("order_id")
        order_status = args.get("order_status")

        record_6 = BillInfo.query.filter_by(order_id=order_id).first()
        if record_6 is None:
            self.logger.error(MsgDesc.h_401_105.value + "<order_id: {}>".format(order_id))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=105, string=MsgDesc.h_401_105.value), 401
        else:
            if record_6.order_status == "已审核":
                # 已审核的订单不允许修改订单的其他数据
                flag = False
                if args.get("country") is not None:
                    flag = True
                if args.get("room_id") is not None:
                    flag = True
                if args.get("distributor_name") is not None:
                    flag = True
                if args.get("certificate_id") is not None:
                    flag = True
                if args.get("full_or_half_bill") is not None:
                    flag = True
                if args.get("contact") is not None:
                    flag = True
                if args.get("introducer_id") is not None:
                    flag = True
                if args.get("introducer_name") is not None:
                    flag = True
                if args.get("superior_id") is not None:
                    flag = True
                if args.get("superior_name") is not None:
                    flag = True
                if args.get("bank_name") is not None:
                    flag = True
                if args.get("bank_account_id") is not None:
                    flag = True
                if args.get("bank_account_name") is not None:
                    flag = True

                if flag:
                    self.logger.error(MsgDesc.h_401_119.value + "<{}>".format(order_id))
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(code=119, string=MsgDesc.h_401_119.value), 401
                # 已审核的订单只允许操作反审核
                else:
                    record_6.order_status = order_status
                    record_7 = User.query.filter_by(distributor_id=record_6.distributor_id).first()
                    if record_7 is not None:
                        record_7.distributor_status = "未审核"
                    try:
                        db.session.commit()
                        db.session.remove()
                        if record_7 is not None:
                            bonus_audit()
                        self.logger.info(self.logger_formatter + " 成功...")
                        return RespBody.custom(result="success", code=200, string=MsgDesc.h_201_200.value), 201
                    except Exception as e:
                        db.session.rollback()
                        db.session.remove()
                        self.logger.error(MsgDesc.h_411_100.value + "Reason: {}".format(e))
                        self.logger.error(self.logger_formatter + " 失败...")
                        return RespBody.custom(string=MsgDesc.h_411_100.value), 411
            elif record_6.order_status == "未审核":
                introducer_id = args.get("introducer_id")
                superior_id = args.get("superior_id")
                room_id = args.get("room_id")

                if introducer_id is not None:
                    # introducer_id 和 superior_id 必须要系统中都要存在
                    record_1 = BillInfo.query.filter_by(distributor_id=introducer_id).first()
                    record_2 = SalesRoom.query.filter_by(distributor_id=introducer_id).first()

                    # 如果 introducer_id 在系统中不存在
                    if record_1 is None and record_2 is None:
                        self.logger.error(MsgDesc.h_401_116.value + "<{}>".format(introducer_id))
                        self.logger.error(self.logger_formatter + " 失败...")
                        return RespBody.custom(code=116, string=MsgDesc.h_401_116.value), 401
                    else:
                        record_6.introducer_id = introducer_id

                if superior_id is not None:
                    record_3 = BillInfo.query.filter_by(distributor_id=superior_id).first()
                    record_4 = SalesRoom.query.filter_by(distributor_id=superior_id).first()

                    # 如果 superior_id 在系统中不存在
                    if record_3 is None and record_4 is None:
                        self.logger.error(MsgDesc.h_401_117.value + "<{}>".format(superior_id))
                        self.logger.error(self.logger_formatter + " 失败...")
                        return RespBody.custom(code=117, string=MsgDesc.h_401_117.value), 401
                    else:
                        record_6.superior_id = superior_id

                if room_id is not None:
                    record_5 = SalesRoom.query.filter_by(room_id=room_id).first()

                    # room_id 在系统中必须存在
                    if record_5 is None:
                        self.logger.error(MsgDesc.h_401_118.value + "<{}>".format(room_id))
                        self.logger.error(self.logger_formatter + " 失败...")
                        return RespBody.custom(code=118, string=MsgDesc.h_401_118.value), 401
                    else:
                        record_6.room_id = room_id

                if args.get("country") is not None:
                    record_6.country = args.get("country")

                if args.get("distributor_name") is not None:
                    record_6.distributor_name = args.get("distributor_name")

                if args.get("certificate_id") is not None:
                    record_6.certificate_id = args.get("certificate_id")

                if args.get("full_or_half_bill") is not None:
                    record_6.full_or_half_bill = args.get("full_or_half_bill")

                if args.get("order_status") is not None:
                    record_6.order_status = args.get("order_status")

                if args.get("contact") is not None:
                    record_6.contact = args.get("contact")

                if args.get("introducer_name") is not None:
                    record_6.introducer_name = args.get("introducer_name")

                if args.get("superior_name") is not None:
                    record_6.superior_name = args.get("superior_name")

                if args.get("bank_name") is not None:
                    record_6.bank_name = args.get("bank_name")

                if args.get("bank_account_id") is not None:
                    record_6.bank_account_id = args.get("bank_account_id")

                if args.get("bank_account_name") is not None:
                    record_6.bank_account_name = args.get("bank_account_name")

                record_8 = False
                if order_status == "已审核":
                    record_8 = User.query.filter_by(distributor_id=record_6.distributor_id).first()
                    if record_8:
                        record_8.distributor_status = "正常"
                try:
                    db.session.commit()
                    db.session.remove()
                    if record_8:
                        bonus_audit()
                    self.logger.info(self.logger_formatter + " 成功...")
                    return RespBody.custom(result="success", code=200, string=MsgDesc.h_201_200.value), 201
                except Exception as e:
                    db.session.rollback()
                    db.session.remove()
                    self.logger.error(MsgDesc.h_411_100.value + "Reason: {}".format(e))
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(string=MsgDesc.h_411_100.value), 411
