# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  utils
# FileName:     paramCheck.py
# Description:  对参数的判断
# Author:       'zhouhanlin'
# CreateDate:   2020/02/19
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
import re


def is_isalnum(string):
    """
    判断字符串是否只包含字母或者数字
    :param string: 需要校验的字符串
    :return: bool
    """
    reg = re.compile(r'^[0-9a-zA-Z]+$')
    result = reg.match(string)
    if result:
        return True
    else:
        return False


def is_isalnum_ex(string):
    """
    判断字符串是否只包含字母，数字或者"_"
    :param string: 需要校验的字符串
    :return: bool
    """
    reg = re.compile(r'^[0-9a-zA-Z\_]+$')
    result = reg.match(string)
    if result:
        return True
    else:
        return False


def is_datetime_str(string):
    """
    判断字符串是否满足格式：2019-09-24 11:20:00
    :param string: 需要校验的字符串
    :return: bool
    """
    reg = re.compile(
        r'^((([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-02-29))\s([0-1]?[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$')
    result = reg.match(string)
    if result:
        return True
    else:
        return False


def is_datet_str(string):
    """
    判断字符串是否满足格式：2019-09-24
    :param string: 需要校验的字符串
    :return: bool
    """
    reg = re.compile(
        r'^((([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-02-29))')
    result = reg.match(string)
    if result:
        return True
    else:
        return False
