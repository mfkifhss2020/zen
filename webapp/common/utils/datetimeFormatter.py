# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  utils
# FileName:     DatetimeFormatter.py
# Description:  时间类型转换
# Author:       'zhouhanlin'
# CreateDate:   2020/02/20
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from time import time, mktime
from datetime import datetime, timedelta


def get_current_to_earlier_hours(coefficient, precision="millisecond"):
    """
    最近三小时的整点时间节点，如2019-09-27 12:00:00, 2019-09-27 11:50:00, 2019-09-27 11:40:00等
    :param str or int coefficient: 要计算的时间系数, '8'
    :param str precision: 精度类型：有second， millisecond， microsecond， 默认millisecond
    :return: list，元素为时间戳字符串
    """
    datetime_object = datetime.now()
    datetime_str = dt_to_str(datetime_object)
    datetime_int_str = datetime_str[:-4] + "0:00"
    int_list = list()
    if isinstance(coefficient, int):
        pass
    else:
        coefficient = int(coefficient)
    y = 0
    for i in range(coefficient * 6 + 1):
        y = y + 10
        times = datetime_operator(datetime_int_str, y, unit="minutes", rule="sub")
        ts = str_to_unix_st(times, precision)
        int_list.append(ts)
    return int_list


def get_current_unix_st(precision="millisecond", time_zone=8):
    """
    当前时间转unix时间戳
    :param str precision: 精度类型：有second， millisecond， microsecond， 默认second
    :param int time_zone: 指定时间，默认8时区
    :return: int
    """
    # 获取东八区当前时间(北京时间) +8
    # 以0时区为基准，东区为"+" ， 西区为"-", 例如：西二区 time_zone=-2， 东九区 time_zone=+9
    current_zone_time = time()
    if time_zone == 8:
        pass
    else:
        current_time = datetime.now()
        if time_zone < 8:
            tmp_time = current_time - timedelta(hours=8 - time_zone)
            current_zone_time = tmp_time.timestamp()
        elif time_zone > 8:
            tmp_time = current_time - timedelta(hours=time_zone - 8)
            current_zone_time = tmp_time.timestamp()

    # 秒级时间戳，10位
    if precision == "second":
        return int(current_zone_time)
    # 毫秒级时间戳，13位
    elif precision == "millisecond":
        return int(round(current_zone_time * 1000))
    # 微秒级时间戳，16位
    elif precision == "microsecond":
        return int(round(current_zone_time * 1000000))


def datetime_operator(datetime_str, coefficient, unit="hours", rule="add"):
    """
    时间计算
    :param str datetime_str: 要计算的时间因子1, '2019-09-20 10:30:10'
    :param str or int coefficient: 要计算的时间系数, '8'
    :param str unit: 系数单位，有days， seconds， microseconds， milliseconds， minutes， hours， weeks
    :param rule: 运算逻辑， 有：add(加)，sub(减)
    :return: str
    """
    datetime_1 = str_to_dt(datetime_str)
    if unit == "days":
        if rule == "add":
            if isinstance(coefficient, int):
                datetime_1 = datetime_1 + timedelta(days=coefficient)
            else:
                datetime_1 = datetime_1 + timedelta(days=int(coefficient))
        elif rule == "sub":
            if isinstance(coefficient, int):
                datetime_1 = datetime_1 - timedelta(days=coefficient)
            else:
                datetime_1 = datetime_1 - timedelta(days=int(coefficient))
    elif unit == "hours":
        if rule == "add":
            if isinstance(coefficient, int):
                datetime_1 = datetime_1 + timedelta(hours=coefficient)
            else:
                datetime_1 = datetime_1 + timedelta(hours=int(coefficient))
        elif rule == "sub":
            if isinstance(coefficient, int):
                datetime_1 = datetime_1 - timedelta(hours=coefficient)
            else:
                datetime_1 = datetime_1 - timedelta(hours=int(coefficient))
    elif unit == "minutes":
        if rule == "add":
            if isinstance(coefficient, int):
                datetime_1 = datetime_1 + timedelta(minutes=coefficient)
            else:
                datetime_1 = datetime_1 + timedelta(minutes=int(coefficient))
        elif rule == "sub":
            if isinstance(coefficient, int):
                datetime_1 = datetime_1 - timedelta(minutes=coefficient)
            else:
                datetime_1 = datetime_1 - timedelta(minutes=int(coefficient))
    elif unit == "seconds":
        if rule == "add":
            if isinstance(coefficient, int):
                datetime_1 = datetime_1 + timedelta(seconds=coefficient)
            else:
                datetime_1 = datetime_1 + timedelta(seconds=int(coefficient))
        elif rule == "sub":
            if isinstance(coefficient, int):
                datetime_1 = datetime_1 - timedelta(seconds=coefficient)
            else:
                datetime_1 = datetime_1 - timedelta(seconds=int(coefficient))
    return dt_to_str(datetime_1)


def dt_to_str(dt_object):
    """
    datetime转字符串
    :param dt_object: datetime类型对象数据
    :return: 时间字符串格式：2019-09-24 11:20:00"
    """
    dt_str = datetime.strftime(dt_object, '%Y-%m-%d %H:%M:%S')
    return dt_str


def dt_to_unix_st(dt_object, precision="millisecond"):
    """
    datetime转unix时间戳
    :param dt_object: datetime类型对象数据
    :param str precision: 精度类型：有second， millisecond， microsecond， 默认second
    :return: 时间戳格式: 1562491441
    """
    time_in_unix = dt_object.timestamp()
    # 秒级时间戳，10位
    if precision == "second":
        return int(time_in_unix)
    # 毫秒级时间戳，13位
    elif precision == "millisecond":
        return int(round(time_in_unix * 1000))
    # 微秒级时间戳，16位
    elif precision == "microsecond":
        return int(round(time_in_unix * 1000000))


def unix_st_to_dt(time_in_unix):
    """
    将unix时间戳转换为datetime
    :param time_in_unix: 时间戳格式: 1562491441
    :return: datetime类型对象数据
    """
    dt_object = datetime.fromtimestamp(time_in_unix)
    return dt_object


def unix_st_to_str(time_in_unix):
    """
    将unix时间戳转换为时间字符串
    :param int or str time_in_unix: 时间戳格式: 1562491441
    :return: 时间字符串格式：2019-09-24 11:20:00"
    """
    if isinstance(time_in_unix, int):
        dt_object = datetime.fromtimestamp(time_in_unix)
    else:
        dt_object = datetime.fromtimestamp(int(time_in_unix))
    dt_str = datetime.strftime(dt_object, '%Y-%m-%d %H:%M:%S')
    return dt_str


def str_to_dt(dt_str):
    """
    字符串转为dateTime类型
    :param dt_str: 时间字符串格式：2019-09-24 11:20:00"
    :return: datetime类型对象数据
    """
    if len(dt_str) == 19:
        dt_object = datetime.strptime(dt_str, '%Y-%m-%d %H:%M:%S')
        return dt_object
    elif len(dt_str) == 10:
        dt_str = dt_str + " 00:00:00"
        dt_object = datetime.strptime(dt_str, '%Y-%m-%d %H:%M:%S')
        return dt_object
    else:
        raise Exception("时间字符串格式不正确.")


def str_to_unix_st(dt_str, precision="millisecond"):
    """
    字符串转为unix时间戳
    :param dt_str: 时间字符串格式：20190924112000"
    :param str precision: 精度类型：有second， millisecond， microsecond， 默认second
    :return: 时间戳格式: 1562491441
    """
    if dt_str.find(":") != -1:
        dt_object = datetime.strptime(dt_str, '%Y-%m-%d %H:%M:%S')
    elif dt_str.isdigit():
        dt_object = datetime.strptime(dt_str, '%Y%m%d%H%M%S')
    time_in_unix = dt_object.timestamp()
    # 秒级时间戳，10位
    if precision == "second":
        return int(time_in_unix)
    # 毫秒级时间戳，13位
    elif precision == "millisecond":
        return int(round(time_in_unix * 1000))
    # 微秒级时间戳，16位
    elif precision == "microsecond":
        return int(round(time_in_unix * 1000000))


def get_day_zero_time(date_time):
    """
    根据日期获取当天凌晨时间
    :param datetime date_time: 某一天日期
    :return: int(时间戳)
    """
    if not date_time:
        return 0
    date_zero = datetime.now().replace(year=date_time.year,
                                       month=date_time.month,
                                       day=date_time.day,
                                       hour=0,
                                       minute=0,
                                       second=0)
    date_zero_time = int(mktime(date_zero.timetuple()))
    return date_zero_time


def get_month_range(start_day, end_day):
    """
    获取两个日期之间的所有月份
    :param start_day: 开始时间
    :param end_day: 结束时间
    :return: 列表
    """
    months = (end_day.year - start_day.year) * 12 + end_day.month - start_day.month
    gen = ((start_day.year + mon // 12, mon % 12 + 1) for mon in range(start_day.month - 1, start_day.month + months))
    month_range = list()
    for i in gen:
        if i[1] < 10:
            month_range.append(str(i[0]) + '-' + "0" + str(i[1]))
        else:
            month_range.append(str(i[0]) + '-' + str(i[1]))
    # month_range = ['%s-%s' % (start_day.year + mon // 12, mon % 12 + 1) for mon in
    #                range(start_day.month - 1, start_day.month + months)]
    return month_range


if __name__ == "__main__":
    # c_time = datetime.now()
    # str_1 = dt_to_str(c_time)
    # print(str_1)
    # st_1 = dt_to_unix_st(c_time)
    # dt_1 = str_to_dt(str_1)
    # print(dt_1)
    # dt_2 = unix_st_to_dt(st_1)
    # print(dt_2)
    # str_1 = "2019-09-27 15:00:00"
    # st_2 = str_to_unix_st(str_1)
    # print(st_2)
    # str_2 = unix_st_to_str(st_1)
    # print(str_2)
    # t_s = get_current_unix_st()
    # t_mil = get_current_unix_st("millisecond")
    # t_mic = get_current_unix_st("microsecond")
    # t_0_s = get_current_unix_st(time_zone=-8)
    # t_0_mil = get_current_unix_st("millisecond", -8)
    # t_0_mic = get_current_unix_st("microsecond", -8)
    # print(t_s)
    # print(t_mil)
    # print(t_mic)
    # print(t_0_s)
    # print(t_0_mil)
    # print(t_0_mic)

    t = datetime_operator("2019-09-27 12:00:00", 8, rule="add")
    print(t)
    # d_int_list = get_current_to_earlier_hours(3)
    # for l in d_int_list:
    #     print(l)
    # str_1 = "2019-09-27 15:00:00"
    # start_t = str_to_dt(str_1)
    # end_t = datetime.now()
    # list_10 = get_month_range(start_t, end_t)
    # print(list_10)
