# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     generateCode.py
# Description:  生成各种需要的机器码
# Author:       'zhouhanlin'
# CreateDate:   2020/02/20
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from time import time, strftime, localtime


def get_order_code():
    """
    根据时间生成订单号(0-9组成的21位序列)
    :return: str
    """
    order_no = str(strftime('%Y%m%d%H%M%S', localtime(time()))) + str(time()).replace('.', '')[-7:]
    return order_no


