# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  utils
# FileName:     wsgi.py
# Description:  wsgi接口
# Author:       zhouhanlin
# CreateDate:   2020/2/18
# Copyright ©2011-2020. Hunan xsyxsc e-Commerce Company limited. All rights reserved.
# -------------------------------------------------------------------------------
"""
import os
import re
from webapp import env
import multiprocessing
from gunicorn.app.base import BaseApplication
from gunicorn.six import iteritems
from configparser import ConfigParser


def number_of_workers():
    return (multiprocessing.cpu_count() * 2) + 1


class StandaloneApplication(BaseApplication):

    def __init__(self, app, conf_file):
        self.conf_file = conf_file
        self.options = self.__config() or {}
        self.application = app
        super(StandaloneApplication, self).__init__()

    def load_config(self):
        config = dict([(key, value) for key, value in iteritems(self.options)
                       if key in self.cfg.settings and value is not None])
        for key, value in iteritems(config):
            self.cfg.set(key.lower(), value)

    def load(self):
        return self.application

    def __config(self):
        if os.path.exists(self.conf_file):
            d = dict()
            conf_gunicorn = ConfigParser()
            conf_gunicorn.read(self.conf_file, encoding='utf8')
            all_sections_list = conf_gunicorn.sections()
            for section in all_sections_list:
                item_list = conf_gunicorn.options(section)
                for item in item_list:
                    value = None
                    if item == "access_log_format":
                        with open(self.conf_file, encoding='utf8') as f:
                            context = f.read()
                            sector_array = re.findall(r"\[([a-zA-Z]+)\]", context)
                            sub_item_string_array = re.split(r"\[[a-zA-Z]+\]", context)
                            for index, sub_sector in enumerate(sector_array):
                                if sub_sector == section:
                                    sub_item_string = sub_item_string_array[index + 1]
                                    sub_item_value = re.findall(r"access_log_format[\s]*=[\s]*'([\S\s]+)'",
                                                                sub_item_string)
                                    if len(sub_item_value) > 0:
                                        value = sub_item_value[0].strip()
                    elif item == "accesslog" or item == "errorlog":
                        rel_path = conf_gunicorn.get(section, item).split("/")[1:]
                        value = env.project + env.separator + "{}".format(env.separator).join(rel_path)
                    else:
                        value = conf_gunicorn.get(section, item)
                    if value.lower() == "false":
                        value = False
                    elif value.lower() == "true":
                        value = True
                    try:
                        value = eval(value)
                    except:
                        pass
                    finally:
                        d['workers'] = number_of_workers()
                        d[item] = value
            return d
        else:
            return None
