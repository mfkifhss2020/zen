# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     utils.py
# Description:  http工具
# Author:       'zhouhanlin'
# CreateDate:   2020/04/06
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
import gevent
from flask import copy_current_request_context


def async_call(func, *args, **kwargs):
    """
    异步任务处理。本函数会立即返回，并使用gevent的新线程执行 func 函数（带request上下文）。
    """
    return gevent.spawn(copy_current_request_context(func), *args, **kwargs)
