# -*- coding: utf-8 -*-#
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  http
# FileName:     decorators.py
# Description:  装饰器模块
# Author:       zhouhanlin
# CreateDate:   2020/02/27
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from functools import wraps
from flask import session, current_app

from webapp.common.http.respBody import RespBody, MsgDesc


def login_required(function):
    """
    登陆限制的装饰器
    :param func function: 形参类型是一个函数，需要被装饰的函数
    :return: self.function 一个内部函数本身
    """

    @wraps(function)
    def wrapper(*args, **kwargs):
        # 如果distributor_id存在
        if "distributor_id" in session.keys():
            if session.get("distributor_id"):
                return function(*args, **kwargs)
            else:
                current_app.logger.error(MsgDesc.h_401_121.value)
                return RespBody.custom(code=121, string=MsgDesc.h_401_121.value), 401
        else:
            current_app.logger.error(MsgDesc.h_401_122.value)
            return RespBody.custom(code=122, string=MsgDesc.h_401_122.value), 401

    return wrapper


def access_auth(function):
    """
    访问授权装饰器
    :param func function: 形参类型是一个函数，需要被装饰的函数
    :return: self.function 一个内部函数本身
    """

    @wraps(function)
    def wrapper(*args, **kwargs):
        # 如果distributor_id存在
        if "distributor_id" in session.keys():
            if session.get("distributor_id") == "admin":
                return function(*args, **kwargs)
            else:
                current_app.logger.error(MsgDesc.h_401_123.value)
                return RespBody.custom(code=123, string=MsgDesc.h_401_123.value), 401
        else:
            current_app.logger.error(MsgDesc.h_401_121.value)
            return RespBody.custom(code=121, string=MsgDesc.h_401_121.value), 401

    return wrapper
