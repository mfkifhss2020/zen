# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     respBody.py
# Description:  HTTP响应描述
# Author:       zhouhanlin
# CreateDate:   2020/03/07
# Copyright ©2011-2020. Hunan xsyxsc e-Commerce Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from enum import Enum
from copy import deepcopy


class MsgDesc(Enum):
    """
    HTTP status说明
    200 响应正常
    201 表单提交成功/数据更新成功
    204 删除成功
    400 请求参数缺失
    401 请求参数错误
    404 查询结果异常
    411 程序处理过程中异常
    h_xxx_xxx说明： 左起第一个xxx表示http status值，第二个xxx表示body里面的code值
    """
    # 查询操作成功
    h_200_200 = "Query operation success."
    # 表单提交成功/数据更新成功
    h_201_200 = "Form submit success, or data updated success."
    # 删除成功
    # h_204_200 = "Data delete success."
    # 参数过多或者参数缺失
    h_400_100 = "Too many or missing parameters."
    # 参数值无效
    h_401_100 = "Invalid parameter value."
    # 查询不到数据
    h_404_100 = "Query success, no data found."
    # 程序运行过程中发生异常
    h_411_100 = "An exception occurred during program running."
    # 服务器内部错误
    h_500_100 = "Internal Server Error."

    # 用户认证通过
    h_201_201 = "Distributor authentication passed."
    # 用户成功退出系统
    h_201_202 = "User logout the system success."
    # 用户密码更新成功
    h_201_204 = "User password changed success."
    # 用户密码重置成功
    h_201_205 = "Password reset success."

    # 未推送的通知不能撤回
    h_401_101 = "Not pushed notice cannot be withdrawn."
    # 已推送的通知不允许再编辑
    h_401_102 = "Published notice do not allow editing."
    # 重复字段值
    h_401_103 = "Duplicate field value."
    # 删除失败，数据不存在
    h_401_104 = "Delete failed, data doesn't exist."
    # 更新失败，数据不存在
    h_401_105 = "Update failed, data doesn't exist."
    # 用户名或者密码错误
    h_401_106 = "Wrong distributor ID or password."
    # 用户状态不正常，不允许登录系统
    h_401_107 = "Distributor status is abnormal, it is not allowed to login."
    # 参数值超长，通知标题不能大于150个字符，通知内容不能大于1000个字符
    h_401_108 = "Value is too long, Title cannot be greater than 150, content cannot be greater than 1000."
    # 密码更新失败，原密码和新密码相同
    h_401_109 = "Update password failed, original password is the same as new password."
    # 输入的原密码不正确
    h_401_110 = "Enter the original password is incorrect."
    # 新密码长度不少于6个字符，不大于16个字符
    h_401_111 = "The new password must be at least 6 and cannot exceed 16."
    # 用户没有下级组织关系
    h_401_112 = "User has no subordinate organization."
    # 未冻结的用户，不能操作解冻
    h_401_113 = "Distributor status isn't frozen, can't thaw."
    # 用户状态非正常，不能操作冻结
    h_401_114 = "Distributor status isn't normal, can't freeze"
    # 无效的时间格式
    h_401_115 = "Datetime illegal, correct format <xxxx-xx-xx xx:xx:xx> or <xxxx-xx-xx>."
    # 介绍人id在系统中不存在
    h_401_116 = "Introducer ID doesn't exist in the system."
    # 上线id在系统中不存在
    h_401_117 = "Superior ID doesn't exist in the system."
    # 门店id在系统中不存在
    h_401_118 = "Store ID doesn't exist in the system."
    # 已审批的单据不允许修改
    h_401_119 = "Order status has been approved, can't be modified."
    # 只能查询最近6个月的数据
    h_401_120 = "The month value is incorrect. Only the data within the last 6 months can be queried."
    # 用户没有登录系统
    h_401_121 = "User is not logged into the system."
    # 会话中出现的用户信息错误
    h_401_122 = "User information error in session."
    # 非管理员用户，不允许访问这个URL
    h_401_123 = "Not admin user, Don't have permission to access this URL."
    # 经销商id格式非法
    h_401_124 = "The distributor id is incorrect."


class RespBody(object):
    success_resp = {
        "code": 200,
        "message": "operation success."
    }

    failed_resp = {
        "code": 100,
        "message": "operation failed."
    }

    @classmethod
    def __formatter(cls, action, result="success", code=None, string=None, data=None,
                    current_page=None, pages=None, per_page=None, total=None):
        if action == "custom":
            if result == "success":
                success_resp = deepcopy(cls.success_resp)
                success_resp['message'] = success_resp['message'].capitalize()
                if code is not None:
                    success_resp['code'] = code
                if string is not None:
                    success_resp['message'] = string
                if data is not None:
                    success_resp['data'] = data
                if current_page is not None and pages is not None and per_page is not None and total is not None:
                    success_resp['per_page'] = per_page
                    success_resp["pages"] = pages
                    success_resp["current_page"] = current_page
                    success_resp['total'] = total
                return success_resp
            elif result == "failed":
                failed_resp = deepcopy(cls.failed_resp)
                failed_resp['message'] = failed_resp['message'].capitalize()
                if code is not None:
                    failed_resp['code'] = code
                if string is not None:
                    failed_resp['message'] = string
                return failed_resp
        else:
            if result == "success":
                success_resp = deepcopy(cls.success_resp)
                success_resp['message'] = "{} ".format(action.capitalize()) + success_resp['message']
                if code is not None:
                    success_resp['code'] = code
                if string is not None:
                    success_resp['message'] = success_resp['message'][:-1] + " " + string
                if data is not None:
                    success_resp['data'] = data
                if current_page is not None and pages is not None and per_page is not None and total is not None:
                    success_resp['per_page'] = per_page
                    success_resp["pages"] = pages
                    success_resp["current_page"] = current_page
                    success_resp['total'] = total
                return success_resp
            elif result == "failed":
                failed_resp = deepcopy(cls.failed_resp)
                failed_resp['message'] = "{} ".format(action.capitalize()) + failed_resp['message']
                if code is not None:
                    failed_resp['code'] = code
                if string is not None:
                    failed_resp['message'] = failed_resp['message'][:-1] + " " + string
                return failed_resp

    @classmethod
    def get(cls, result="success", code=None, string=None, data=None,
            current_page=None, pages=None, per_page=None, total=None):
        return cls.__formatter("get", result=result, code=code, string=string, data=data,
                               current_page=current_page, pages=pages, per_page=per_page, total=total)

    @classmethod
    def post(cls, result="success", code=None, string=None, data=None):
        return cls.__formatter("post", result=result, code=code, string=string, data=data)

    @classmethod
    def put(cls, result="success", code=None, string=None, data=None):
        return cls.__formatter("put", result=result, code=code, string=string, data=data)

    @classmethod
    def delete(cls, result="success", code=None, string=None, data=None):
        return cls.__formatter("delete", result=result, code=code, string=string, data=data)

    @classmethod
    def custom(cls, result="failed", code=None, string=None, data=None,
               current_page=None, pages=None, per_page=None, total=None):
        return cls.__formatter("custom", result=result, code=code, string=string, data=data,
                               current_page=current_page, pages=pages, per_page=per_page, total=total)
