# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  logging
# FileName:     exceptions.py
# Description:  自定义异常类
# Author:       zhouhanlin
# CreateDate:   2020/2/18
# Copyright ©2011-2020. Hunan xsyxsc e-Commerce Company limited. All rights reserved.
# -------------------------------------------------------------------------------
"""


class BaseLockException(Exception):
    # Error codes:
    LOCK_FAILED = 1

    def __init__(self, *args, **kwargs):
        self.fh = kwargs.pop('fh', None)
        Exception.__init__(self, *args, **kwargs)


class LockException(BaseLockException):
    pass


class AlreadyLocked(BaseLockException):
    pass


class FileToLarge(BaseLockException):
    pass
