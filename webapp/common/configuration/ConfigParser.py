# -*- coding: utf-8 -*-
"""
# -------------------------------------------------------------------------------
# ProjectName:  configuration
# FileName:     ConfigParser.py
# Description:  解析配置文件
# Author:       zhouhanlin
# CreateDate:   2020/2/18
# Copyright ©2011-2020. Hunan xsyxsc e-Commerce Company limited. All rights reserved.
# -------------------------------------------------------------------------------
"""
import re
import configparser


class ConfigParser(object):
    """
    解析配置文件
    """

    def __init__(self, cfg_file):
        self.value = None
        self.cfg_file = cfg_file

    def get_config(self, sector, item):
        cf = configparser.ConfigParser()
        try:
            cf.read(self.cfg_file, encoding='utf8')
            if item == "formatter":
                with open(self.cfg_file, encoding='utf8') as f:
                    context = f.read()
                    sector_array = re.findall(r"\[([a-zA-Z]+)\]", context)
                    sub_item_string_array = re.split(r"\[[a-zA-Z]+\]", context)
                    for index, sub_sector in enumerate(sector_array):
                        if sub_sector == sector:
                            sub_item_string = sub_item_string_array[index + 1]
                            sub_item_value = re.findall(r'formatter =[\s]+"([\S\s]+)"', sub_item_string)
                            if len(sub_item_value) > 0:
                                self.value = sub_item_value[0].strip()

            else:
                self.value = cf.get(sector, item)
        except KeyError:
            print(KeyError)
        finally:
            return self.value


if __name__ == "__main__":
    log_properties = r'D:\workspace\python\XsyxOps\conf\log.properties'
    conf = ConfigParser("{}".format(log_properties))
    formatter = conf.get_config("flask", "formatter")
    max_bytes = conf.get_config("flask", "max_bytes")
    back_count = conf.get_config("flask", "back_count")
    print(formatter)
    print([eval(max_bytes)])
    print([eval(back_count)])
