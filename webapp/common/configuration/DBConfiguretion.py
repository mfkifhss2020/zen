# -*- coding: utf-8 -*-
"""
# -------------------------------------------------------------------------------
# ProjectName:  configuration
# FileName:     DbConfiguretion.py
# Description:  数据库初始化配置
# Author:       zhouhanlin
# CreateDate:   2020/2/18
# Copyright ©2011-2020. Hunan xsyxsc e-Commerce Company limited. All rights reserved.
# -------------------------------------------------------------------------------
"""
from .ConfigParser import ConfigParser


class DbInitConfig(object):
    """
    DB配置初始化
    """

    def __init__(self, properties_file, logger, sector):
        self.db_uri = None
        self.logger = logger
        # 获取数据库相关信息
        self.properties_file = properties_file
        self.sector = sector
        self.conf = ConfigParser("{}".format(self.properties_file))
        self.dialect = self.conf.get_config('{}'.format(self.sector), "dialect")
        self.driver = self.conf.get_config('{}'.format(self.sector), "driver")
        self.username = self.conf.get_config('{}'.format(self.sector), "username")
        self.password = self.conf.get_config('{}'.format(self.sector), "password")
        self.host = self.conf.get_config('{}'.format(self.sector), "host")
        self.port = self.conf.get_config('{}'.format(self.sector), "port")
        self.database = self.conf.get_config('{}'.format(self.sector), "database")

    @property
    def sql_alchemy_conn(self):
        if self.dialect and self.username and self.password and self.host and self.port and self.database:
            self.db_uri = "{}+{}://{}:{}@{}:{}/{}?charset=utf8".format(self.dialect, self.driver, self.username,
                                                                       self.password, self.host, self.port,
                                                                       self.database)
            return self.db_uri
        else:
            self.logger.error("DB properties中{}区域配置存在异常......".format(self.sector))
