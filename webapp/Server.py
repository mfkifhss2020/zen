# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     Server.py
# Description:  服务入口模块
# Author:       'zhouhanlin'
# CreateDate:   2020/02/19
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
import os
import platform
from flask_cors import CORS
from datetime import timedelta
from flask import Flask, make_response
from flask.logging import default_handler

from webapp import env, db, scheduler
from webapp.bluePrint import blueprint
from webapp.task.scheduleTask import SchedulerTask
from webapp.common.logging import MultiprocessSizeRotatingLog
from webapp.common.configuration.DBConfiguretion import DbInitConfig


def create_app():
    log_param = env.conf + env.separator + "log.properties"
    db_param = env.conf + env.separator + "db.properties"

    # 初始化APP对象
    app = Flask(__name__, static_folder=env.static, template_folder=env.templates)
    app.register_blueprint(blueprint, url_prefix='/api/v1')

    # 调式模式切换
    os.environ["FLASK_ENV"] = 'production'
    # os.environ["FLASK_ENV"] = 'development'
    logger = MultiprocessSizeRotatingLog(log_param, "zen", env.project, env.logs, separator=env.separator).get_log

    # 初始化日志对象
    app.logger.removeHandler(default_handler)
    app.logger = logger

    # json格式化输出
    app.config["JSONIFY_PRETTYPRINT_REGULAR"] = True

    # json输出中文使用utf8编码
    app.config.update(RESTFUL_JSON=dict(ensure_ascii=False))

    # session配置
    # 添加1个24位的随机数，用于flask加密的盐
    app.config["SECRET_KEY"] = os.urandom(24)
    # 设置session有效时长, PERMANENT_SESSION_LIFETIME 参数默认是31天
    app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(days=7)

    # 连接数据库
    db_uri = DbInitConfig(db_param, app.logger, "mariadb").sql_alchemy_conn
    if db_uri:
        app.config["SQLALCHEMY_DATABASE_URI"] = db_uri
        app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
        db.init_app(app)

    # 为实例化的flask引入定时任务配置
    app.config.from_object(SchedulerTask())

    # 把任务列表载入实例flask
    scheduler.init_app(app)
    # 启动任务计划
    scheduler.start()

    return app


if __name__ == "__main__":
    import gevent.monkey

    gevent.monkey.patch_all()

    application = create_app()


    @application.after_request
    def http_response(resp):
        """
        HTTP响应头拦截器，在所有的请求发生后执行，加入headers。
        :param response resp: http响应对象
        :return: response对象
        """
        response = make_response(resp)
        response.headers['Access-Control-Expose-Headers'] = 'Set-Cookie'
        return response


    # 支持本地域，远程域名访问
    CORS(application, supports_credentials=True)

    if "Windows" in platform.system():
        application.run(host="0.0.0.0", port=5051)
    else:
        from webapp.common.utils.wsgi import StandaloneApplication

        g_file = env.conf + env.separator + "gunicorn.properties"
        StandaloneApplication(application, g_file).run()
