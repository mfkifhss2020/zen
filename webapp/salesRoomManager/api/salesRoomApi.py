# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     salesRoomApi.py
# Description:  专卖店信息增删改查api接口
# Author:       'zhouhanlin'
# CreateDate:   2020/02/20
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""

import pymysql.err
import sqlalchemy.exc
from flask import current_app, request
from flask_restful import Resource, fields, reqparse, marshal

from webapp import db
from webapp.userManager.models.User import User
from webapp.common.utils.paramCheck import is_isalnum
from webapp.billManager.models.billInfo import BillInfo
from webapp.bonusTotal.utils.bonusCalc import bonus_audit
from webapp.common.http.respBody import RespBody, MsgDesc
from webapp.salesRoomManager.models.salesRoom import SalesRoom
from webapp.common.http.decorators import login_required, access_auth

query_fields = {
    'key': fields.String,
    'room_id': fields.String,
    'country': fields.String,
    'distributor_id': fields.String,
    'shopkeeper_name': fields.String,
    'certificate_id': fields.String,
    'contact_1': fields.String,
    'contact_2': fields.String,
    'contact_3': fields.String,
    'create_time': fields.String,
    'update_time': fields.String
}


class SalesRoomApi(Resource):

    # decorators = [check_http_headers, check_request_frequency]
    def __init__(self):
        self.__env = request.environ
        self.remote_ip = self.__env.get("REMOTE_ADDR")
        self.remote_port = self.__env.get("REMOTE_PORT")
        self.protocol = self.__env.get("SERVER_PROTOCOL").split("/")[0].lower()
        self.http_host = self.__env.get("HTTP_HOST")
        self.method = self.__env.get("REQUEST_METHOD")
        self.uri = self.__env.get("PATH_INFO")
        self.access_user = request.remote_user
        self.logger = current_app.logger

        if self.access_user:
            self.logger_formatter = "来自IP：<{}>".format(self.remote_ip) + \
                                    "的端口：[{}]的 {} 用户".format(self.remote_port, self.access_user) + \
                                    "通过 {} ".format(self.method) + \
                                    "方法访问url：{}".format(self.protocol) + "//" + \
                                    "{}".format(self.http_host) + \
                                    "{}".format(self.uri)
        else:
            self.logger_formatter = "来自IP：<{}>".format(self.remote_ip) + \
                                    "的端口：[{}]".format(self.remote_port) + \
                                    "通过 {} ".format(self.method) + \
                                    "方法访问url：{}".format(self.protocol) + "//" + \
                                    "{}".format(self.http_host) + \
                                    "{}".format(self.uri)

    @login_required
    @access_auth
    def get(self):
        """
        专卖店信息全量以及模糊查询
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取args中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("current_page", type=int, help="当前页数校验错误", required=True, location='args')
        parse.add_argument("per_page", type=int,
                           help="必须设置页码条目数为10，20，50，或者100",
                           choices=[10, 20, 50, 100],
                           required=True,
                           location='args'
                           )

        parse.add_argument("keyword", type=str, required=False, location='args')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        keyword = args.get("keyword")
        current_page = args.get("current_page")
        per_page = args.get("per_page")
        if keyword is not None:
            filter_keyword = "%%" + keyword + "%%"
            record = SalesRoom.query.filter(
                SalesRoom.room_id.like(filter_keyword) |
                SalesRoom.country.like(
                    filter_keyword) | SalesRoom.distributor_id.like(
                    filter_keyword) | SalesRoom.contact_1.like(filter_keyword)).order_by(
                SalesRoom.create_time.desc()).all()
            total = len(record)
            if total == 0:
                self.logger.error(MsgDesc.h_404_100.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_404_100.value), 404
            else:
                if total % per_page == 0:
                    pages = total // per_page
                else:
                    pages = total // per_page + 1
                # 如果请求的页面数大于数据的总页面数，将报错
                if current_page <= pages:
                    offset_start = (current_page - 1) * per_page
                    offset_end = current_page * per_page
                    self.logger.info(self.logger_formatter + " 成功...")
                    return RespBody.custom(result="success",
                                           code=200,
                                           string=MsgDesc.h_200_200.value,
                                           data=marshal(record[offset_start:offset_end], query_fields),
                                           current_page=current_page,
                                           per_page=per_page,
                                           pages=pages,
                                           total=total), 200
                else:
                    self.logger.error(MsgDesc.h_400_100.value)
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(string=MsgDesc.h_400_100.value), 400
        else:
            # 根据创建时间降序排列
            data = SalesRoom.query.order_by(SalesRoom.create_time.desc()).all()
            total = len(data)
            if total > 0:
                if total % per_page == 0:
                    pages = total // per_page
                else:
                    pages = total // per_page + 1
                # 如果请求的页面数大于数据的总页面数，将报错
                if current_page <= pages:
                    offset_start = (current_page - 1) * per_page
                    offset_end = current_page * per_page
                    self.logger.info(self.logger_formatter + " 成功...")
                    return RespBody.custom(result="success",
                                           code=200,
                                           string=MsgDesc.h_200_200.value,
                                           data=marshal(data[offset_start:offset_end], query_fields),
                                           current_page=current_page,
                                           per_page=per_page,
                                           pages=pages,
                                           total=total), 200
                else:
                    self.logger.error(MsgDesc.h_401_100.value)
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(string=MsgDesc.h_401_100.value), 401
            else:
                self.logger.error(MsgDesc.h_404_100.value)
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_404_100.value), 404

    @login_required
    @access_auth
    def post(self):
        """
        新增专卖店信息
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取form中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("room_id", type=str, help='门店id验证错误', required=True, location='form')
        parse.add_argument("country", type=str, help='国家验证错误', required=True, location='form')
        parse.add_argument("distributor_id", type=str, help='经销商id验证错误', required=True, location='form')
        parse.add_argument("shopkeeper_name", type=str, help='店主姓名验证错误', required=True, location='form')
        parse.add_argument("certificate_id", type=str, help='证件号验证错误', required=True, location='form')

        parse.add_argument("contact_1", type=str, help='联系方式1验证错误', required=False, location='form')
        parse.add_argument("contact_2", type=str, help='联系方式2验证错误', required=False, location='form')
        parse.add_argument("contact_3", type=str, help='联系方式3验证错误', required=False, location='form')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        # 生成组合键
        room_id = args.get("room_id")
        country = args.get("country")
        distributor_id = args.get("distributor_id")

        if not is_isalnum(room_id):
            self.logger.error(MsgDesc.h_401_100.value + "<{}> Only letters or numbers.".format(room_id))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(string=MsgDesc.h_401_100.value), 401

        if not is_isalnum(distributor_id):
            self.logger.error(MsgDesc.h_401_100.value + "<{}> Only letters or numbers.".format(distributor_id))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(string=MsgDesc.h_401_100.value), 401

        if not is_isalnum(country):
            self.logger.error(MsgDesc.h_401_100.value + "<{}> Only letters or numbers.".format(country))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(string=MsgDesc.h_401_100.value), 401

        key = country + "_" + distributor_id + "_" + room_id

        # 判断distributor_id在bill_info表中是否存在
        record_1 = BillInfo.query.filter_by(distributor_id=distributor_id).first()
        if record_1:
            self.logger.error(MsgDesc.h_401_103.value + "<{}>".format(distributor_id))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=103, string=MsgDesc.h_401_103.value), 401

        # 如果distributor_id在user中不存在，则需要将distributor_id插入user表
        record = User.query.filter_by(distributor_id=distributor_id).first()
        if record is None:
            # 生成用户的默认密码
            sub_str = distributor_id[-6:]
            user = User(distributor_id=distributor_id,
                        password=sub_str)
            db.session.add(user)

        sales_room = SalesRoom(key=key,
                               room_id=room_id,
                               country=country,
                               distributor_id=distributor_id,
                               shopkeeper_name=args.get("shopkeeper_name"),
                               certificate_id=args.get("certificate_id"),
                               contact_1=args.get("contact_1"),
                               contact_2=args.get("contact_2"),
                               contact_3=args.get("contact_3")
                               )

        db.session.add(sales_room)
        try:
            db.session.commit()
            db.session.remove()
            bonus_audit()
            self.logger.info(self.logger_formatter + " 成功...")
            return RespBody.custom(result="success", code=200, string=MsgDesc.h_201_200.value), 201
        except (pymysql.err.IntegrityError, sqlalchemy.exc.IntegrityError) as e:
            db.session.rollback()
            db.session.remove()
            self.logger.error(MsgDesc.h_401_103.value + "<{}>".format(e))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=103, string=MsgDesc.h_401_103.value), 401
        except Exception as e:
            db.session.rollback()
            db.session.remove()
            self.logger.error(MsgDesc.h_411_100.value + "Reason: {}".format(e))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(string=MsgDesc.h_411_100.value), 411

    @login_required
    @access_auth
    def delete(self):
        """
        删除单条专卖店信息
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取form中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("key", type=str, help='<国家_经销商id_店铺>组合主键验证错误', required=True, location='form')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        key = args.get("key")
        record_1 = SalesRoom.query.filter_by(key=key).first()
        if record_1 is None:
            self.logger.error(MsgDesc.h_401_104.value + "<key: {}>".format(key))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=104, string=MsgDesc.h_401_104.value), 401
        else:
            db.session.delete(record_1)
            # 查看刚删除的经销商店铺是不是，该经销商录入的最后一个店铺
            # 如果是，则同时删除User表中的相关数据
            distributor_id = key.split("_")[1]
            record_2 = SalesRoom.query.filter_by(distributor_id=distributor_id).first()
            record_3 = False
            if record_2 is None:
                record_3 = User.query.filter_by(distributor_id=distributor_id).first()
                if record_3:
                    db.session.delete(record_3)
            try:
                db.session.commit()
                db.session.remove()
                if record_3:
                    bonus_audit()
                self.logger.info(self.logger_formatter + " 成功...")
                return '', 204
            except Exception as e:
                db.session.rollback()
                db.session.remove()
                self.logger.error(MsgDesc.h_411_100.value + "Reason: {}".format(e))
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_411_100.value), 411

    @login_required
    @access_auth
    def put(self):
        """
        api的增加数据方法
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取form中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("key", type=str, help='<国家_经销商id_店铺>组合主键验证错误', required=True, location='form')
        parse.add_argument("room_id", type=str, help='门店id验证错误', required=False, location='form')
        parse.add_argument("country", type=str, help='国家验证错误', required=False, location='form')
        parse.add_argument("distributor_id", type=str, help='经销商id验证错误', required=False, location='form')
        parse.add_argument("shopkeeper_name", type=str, help='店主姓名验证错误', required=False, location='form')
        parse.add_argument("certificate_id", type=str, help='证件号验证错误', required=False, location='form')

        parse.add_argument("contact_1", type=str, help='联系方式1验证错误', required=False, location='form')
        parse.add_argument("contact_2", type=str, help='联系方式2验证错误', required=False, location='form')
        parse.add_argument("contact_3", type=str, help='联系方式3验证错误', required=False, location='form')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        # 取出组合键
        key = args.get("key")
        room_id = args.get("room_id")
        country = args.get("country")
        distributor_id = args.get("distributor_id")
        key_temp = key

        # 如果room_id 传递了新值
        if room_id is not None:
            if not is_isalnum(room_id):
                self.logger.error(MsgDesc.h_401_100.value + "<{}> Only letters or numbers.".format(room_id))
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_401_100.value), 401
            else:
                key_temp_list = key_temp.split("_")
                key_temp_list[2] = room_id
                key_temp = "_".join(key_temp_list)

        # 如果distributor_id 传递了新值
        if distributor_id is not None:
            if not is_isalnum(distributor_id):
                self.logger.error(MsgDesc.h_401_100.value + "<{}> Only letters or numbers.".format(distributor_id))
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_401_100.value), 401
            else:
                key_temp_list = key_temp.split("_")
                key_temp_list[1] = distributor_id
                key_temp = "_".join(key_temp_list)

        # 如果country 传递了新值
        if country is not None:
            if not is_isalnum(country):
                self.logger.error(MsgDesc.h_401_100.value + "<{}> Only letters or numbers.".format(country))
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(string=MsgDesc.h_401_100.value), 401
            else:
                key_temp_list = key_temp.split("_")
                key_temp_list[0] = country
                key_temp = "_".join(key_temp_list)
        record_1 = True
        record = SalesRoom.query.filter_by(key=key).first()
        # 比较原始key和有没重新组合的key，如果没有，则数据不用更新原始key
        if key == key_temp:
            if record is None:
                self.logger.error(MsgDesc.h_401_105.value + "<key: {}>".format(key))
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(code=105, string=MsgDesc.h_401_105.value), 401
            else:
                if args.get("shopkeeper_name") is not None:
                    record.shopkeeper_name = args.get("shopkeeper_name")
                if args.get("certificate_id") is not None:
                    record.certificate_id = args.get("certificate_id")
                if args.get("contact_1") is not None:
                    record.contact_1 = args.get("contact_1")
                if args.get("contact_2") is not None:
                    record.contact_2 = args.get("contact_2")
                if args.get("contact_3") is not None:
                    record.contact_3 = args.get("contact_3")
        # 否则要更新数据的key
        else:
            if record is None:
                self.logger.error(MsgDesc.h_401_105.value + "<key: {}>".format(key))
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(code=105, string=MsgDesc.h_401_105.value), 401
            else:
                # 判断distributor_id在bill_info表中是否存在
                record_2 = BillInfo.query.filter_by(distributor_id=distributor_id).first()
                if record_2:
                    self.logger.error(MsgDesc.h_401_103.value + "<{}>".format(distributor_id))
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(code=103, string=MsgDesc.h_401_103.value), 401
                record_1 = User.query.filter_by(distributor_id=distributor_id).first()
                record.key = key_temp
                if args.get("room_id") is not None:
                    record.room_id = args.get("room_id")
                if args.get("country") is not None:
                    record.country = args.get("country")
                if args.get("distributor_id") is not None:
                    record.distributor_id = args.get("distributor_id")
                if args.get("shopkeeper_name") is not None:
                    record.shopkeeper_name = args.get("shopkeeper_name")
                if args.get("contact_1") is not None:
                    record.contact_1 = args.get("contact_1")
                if args.get("contact_2") is not None:
                    record.contact_2 = args.get("contact_2")
                if args.get("contact_3") is not None:
                    record.contact_3 = args.get("contact_3")
                # 更新参数带来的distributor_id如果在user表中不存在，则需要将distributor_id插入user表
                if not record_1:
                    # 生成用户的默认密码
                    sub_str = distributor_id[-6:]
                    user = User(distributor_id=distributor_id,
                                password=sub_str)
                    db.session.add(user)
        try:
            db.session.commit()
            db.session.remove()
            if not record_1:
                bonus_audit()
            self.logger.info(self.logger_formatter + " 成功...")
            return RespBody.custom(result="success", code=200, string=MsgDesc.h_201_200.value), 201
        except Exception as e:
            db.session.rollback()
            db.session.remove()
            self.logger.error(MsgDesc.h_411_100.value + "Reason: {}".format(e))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(string=MsgDesc.h_411_100.value), 411
