# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     userSubOrgApi.py
# Description:  用户下级组织关系Api
# Author:       'zhouhanlin'
# CreateDate:   2020/02/28
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
import json
from datetime import datetime
from flask_restful import Resource, reqparse
from flask import current_app, request, session

from webapp.common.http.decorators import login_required
from webapp.common.http.respBody import RespBody, MsgDesc
from webapp.bonusTotal.models.bonusSummary import BonusSummary


class UserSubOrg(Resource):

    # decorators = [check_http_headers, check_request_frequency]
    def __init__(self):
        self.__env = request.environ
        self.remote_ip = self.__env.get("REMOTE_ADDR")
        self.remote_port = self.__env.get("REMOTE_PORT")
        self.protocol = self.__env.get("SERVER_PROTOCOL").split("/")[0].lower()
        self.http_host = self.__env.get("HTTP_HOST")
        self.method = self.__env.get("REQUEST_METHOD")
        self.uri = self.__env.get("PATH_INFO")
        self.access_user = request.remote_user
        self.logger = current_app.logger

        if self.access_user:
            self.logger_formatter = "来自IP：<{}>".format(self.remote_ip) + \
                                    "的端口：[{}]的 {} 用户".format(self.remote_port, self.access_user) + \
                                    "通过 {} ".format(self.method) + \
                                    "方法访问url：{}".format(self.protocol) + "//" + \
                                    "{}".format(self.http_host) + \
                                    "{}".format(self.uri)
        else:
            self.logger_formatter = "来自IP：<{}>".format(self.remote_ip) + \
                                    "的端口：[{}]".format(self.remote_port) + \
                                    "通过 {} ".format(self.method) + \
                                    "方法访问url：{}".format(self.protocol) + "//" + \
                                    "{}".format(self.http_host) + \
                                    "{}".format(self.uri)

    # 装饰器判断用户是否已经登录
    @login_required
    def get(self):
        """
        查询经销商下级组织关系
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        # 获得distributor_id
        distributor_id = session.get("distributor_id")
        # 获取当前月份数
        month = datetime.now().strftime('%Y-%m')

        record = BonusSummary.query.filter_by(distributor_id=distributor_id, total_month=month).first()

        if record is None:
            self.logger.error(MsgDesc.h_401_112.value + "<{}>".format(distributor_id))
            self.logger.error(self.logger_formatter + " 失败...")
            return RespBody.custom(code=112, string=MsgDesc.h_401_112.value), 401
        else:
            org_dict = json.loads(record.sub_relation.decode())
            self.logger.info(self.logger_formatter + " 成功...")
            return RespBody.custom(result="success", code=200, string=MsgDesc.h_200_200.value, data=[org_dict]), 200
