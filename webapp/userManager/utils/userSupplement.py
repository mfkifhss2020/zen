# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     userSupplement.py
# Description:  用户数据丰富
# Author:       'zhouhanlin'
# CreateDate:   2020/02/28
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
import pandas as pd
from flask import current_app
from pandas.io.sql import execute
from datetime import datetime, timedelta

from webapp import scheduler, db
from webapp.common.security.encryptAndDecrypt import aes_crypt


# 设置DataFrame打印属性
# pd.set_option('display.max_rows', 5000)
# pd.set_option('display.max_columns', 500)
# pd.set_option('display.width', 1000)


def user_audit():
    try:
        with scheduler.app.app_context():
            current_app.logger.info("开始执行定时任务：< user_audit >, 用户状态审计...")
            sql_1 = "SELECT distributor_id, distributor_status, create_time FROM " + \
                    "t_user_info where distributor_id != 'admin' order by create_time asc;"
            sql_2 = "SELECT distributor_id, full_or_half_bill, superior_id, create_time " + \
                    "FROM t_bill_info order by create_time asc;"
            df_user_info = pd.read_sql(sql_1, con=db.engine)
            df_bill_info = pd.read_sql(sql_2, con=db.engine)
            cancel_distributor_list = get_cancel_distributor(df_user_info, df_bill_info)
            if len(cancel_distributor_list) > 0:
                for cancel_distributor in cancel_distributor_list:
                    status = cancel_distributor.get("distributor_status")
                    distributor_id = cancel_distributor.get("distributor_id")
                    sql_3 = "update t_user_info set " + \
                            "distributor_status='{}' where distributor_id='{}';".format(status, distributor_id)
                    execute(sql_3, db.engine)
            current_app.logger.info("定时任务：< user_audit >执行结束...")
    except Exception as e:
        current_app.logger.error("定时任务：< user_audit >执行失败，原因: {}".format(e))


def get_cancel_distributor(df_user_info, df_bill_info):
    """
    获取用户名下365天内没有产生一笔全单的用户
    :param DataFrame df_user_info: 用户信息表
    :param DataFrame df_bill_info: 订单信息表
    :return: list
    """
    # 1. 获取当前时间到365天以前的起止时间
    current_time = datetime.now()
    start_time = current_time - timedelta(days=365)
    df_bill_info = df_bill_info.astype(dtype={"create_time": "object"})
    distributor_lists = list()
    for index, row in df_user_info.iterrows():
        distributor_id = row['distributor_id']
        distributor_status = row["distributor_status"]
        create_time = row['create_time']
        # 2. 必须满足注册时长等于或者大于365天的经销商
        if create_time <= start_time:
            distributor_dict = dict()
            # 2. 检索经销商365内有无产生全单记录
            df_sub = df_bill_info[
                df_bill_info['superior_id'].isin([distributor_id]) & df_bill_info['full_or_half_bill'].isin(
                    ['是']) & (df_bill_info['create_time'] >= start_time)]
            # 3. 如果没有记录且状态是正常， 则将状态设置为已取消
            if df_sub.empty and distributor_status == "正常":
                distributor_dict["distributor_id"] = distributor_id
                distributor_dict["distributor_status"] = "已取消"
                distributor_lists.append(distributor_dict)
            # 4. 如果有记录且状态是已取消， 则将状态设置为正常
            elif not df_sub.empty and distributor_status == "已取消":
                distributor_dict["distributor_id"] = distributor_id
                distributor_dict["distributor_status"] = "正常"
                distributor_lists.append(distributor_dict)
            else:
                continue
        # 6. 注册时长不满1年的经销商不进行状态检索
        else:
            continue
    return distributor_lists


def rich_user_info(current_page, per_page, keyword=None):
    """
    丰富用户信息表
    :param int current_page: 分页时当前查询页数
    :param int per_page: 每页显示的条目数
    :param str keyword: 模糊查询关键字，默认无
    :return: dict
    """
    split_page = {"code": 100,
                  "data": list(),
                  "pages": 1,
                  "total": 0
                  }

    # 1. 查询三张表的基本信息
    sql_1 = "select distributor_id, shopkeeper_name as distributor_name, certificate_id, " + \
            "contact_1 as contact, create_time from t_sales_room order by create_time desc;"
    sql_2 = "select distributor_id, distributor_name, certificate_id, " + \
            "contact, create_time from t_bill_info order by create_time desc;"
    sql_3 = "select distributor_id, distributor_status, password, create_time " + \
            "from t_user_info where distributor_id != 'admin' order by create_time desc;"
    df_shop_dis = pd.read_sql(sql_1, con=db.engine)
    df_bill_info = pd.read_sql(sql_2, con=db.engine)
    df_user_info = pd.read_sql(sql_3, con=db.engine)
    if df_user_info.empty:
        split_page["status"] = 404
        split_page["code"] = 100
        return split_page

    # 2. 首先根据df_shop_dis的distributor_id去重数据，保留第一条
    df_user_info_1 = df_shop_dis.drop_duplicates(subset=["distributor_id"],
                                                 keep='first',
                                                 inplace=False)
    # 3. 将门店信息表和订单信息表数据合并
    df_user_info_2 = df_user_info_1.append(df_bill_info, ignore_index=True)
    """
    sort_values 排序参数说明
        by:	         指定列名(axis=0或’index’)或索引值(axis=1或’columns’)
        axis:	     若axis=0或’index’，则按照指定列中数据大小排序；若axis=1或’columns’，则按照指定索引中数据大小排序，默认axis=0
        ascending:	 是否按指定列的数组升序排列，默认为True，即升序排列
        inplace:	 是否用排序后的数据集替换原来的数据，默认为False，即不替换
        na_position: {‘first’,‘last’}，设定缺失值的显示位置, 默认是last
    """
    df_user_info_2.sort_values(by=['create_time'], ascending=False, inplace=True)
    df_user_info_2.drop_duplicates(subset=["distributor_id"],
                                   keep='first',
                                   inplace=True)
    df_user_info_2.drop(['create_time'], axis=1, inplace=True)
    # 4. 将基础用户信息表与门店信息表数据关联
    df_user_info_3 = pd.merge(df_user_info, df_user_info_2, how='left', on='distributor_id')

    # 5. 将密码字段进行解密操作
    df_user_info_3['password'] = df_user_info_3['password'].apply(lambda x: aes_crypt.decrypt(x))
    # 6. 将datetime时间转成date时间
    df_user_info_3['create_time'] = df_user_info_3['create_time'].apply(lambda x: datetime.strftime(x, "%Y-%m-%d"))

    if keyword is not None:
        # 可以根据 distributor_id, distributor_name 模糊搜索
        df_sub = df_user_info_3[
            df_user_info_3['distributor_id'].str.contains(keyword) | df_user_info_3['distributor_name'].str.contains(
                keyword)]
        if df_sub.empty:
            split_page["status"] = 404
            split_page["code"] = 100
            return split_page
        else:
            # 总用户数
            total_user = df_sub.shape[0]
            if total_user % per_page == 0:
                split_page["pages"] = total_user // per_page
            else:
                split_page["pages"] = total_user // per_page + 1
            if current_page > split_page["pages"]:
                split_page["status"] = 400
                split_page["code"] = 100
                split_page.pop('pages')
                return split_page
            offset_start = (current_page - 1) * per_page
            offset_end = current_page * per_page
            # DataFrame数据按index取offset_start到offset_end区间的数据
            df_sub = df_sub.iloc[offset_start:offset_end]

            # 将DataFrame转化成列表
            user_info_lists = df_sub.to_dict('records')
            split_page['data'] = user_info_lists
            split_page['current_page'] = current_page
            split_page['per_page'] = per_page
            split_page['total'] = total_user
            return split_page
    else:
        # 总用户数
        total_user = df_user_info_3.shape[0]
        if total_user % per_page == 0:
            split_page["pages"] = total_user // per_page
        else:
            split_page["pages"] = total_user // per_page + 1
        if current_page > split_page["pages"]:
            split_page["status"] = 400
            split_page["code"] = 100
            split_page.pop('pages')
            return split_page
        offset_start = (current_page - 1) * per_page
        offset_end = current_page * per_page
        # DataFrame数据按index取offset_start到offset_end区间的数据
        df_user_info_3 = df_user_info_3.iloc[offset_start:offset_end]

        user_info_lists = df_user_info_3.to_dict('records')
        split_page['data'] = user_info_lists
        split_page['current_page'] = current_page
        split_page['per_page'] = per_page
        split_page['total'] = total_user

        return split_page


def base_user_info(keyword):
    """
    获取基础用户信息
    :param str keyword: 模糊查询关键字
    :return: dict
    """
    result = {"flag": False,
              "data": None
              }

    # 1. 查询两张表的基本信息
    sql_1 = "select distributor_id, shopkeeper_name as distributor_name" + " from t_sales_room;"
    sql_2 = "select distributor_id, distributor_name" + " from t_bill_info;"
    df_shop_dis = pd.read_sql(sql_1, con=db.engine)
    df_bill_info = pd.read_sql(sql_2, con=db.engine)

    # 2. 首先根据df_shop_dis的distributor_id去重数据，保留第一条
    df_user_info_1 = df_shop_dis.drop_duplicates(subset=["distributor_id"],
                                                 keep='first',
                                                 inplace=False)
    # 3. 将门店信息表和订单信息表数据合并
    df_user_info_2 = df_user_info_1.append(df_bill_info, ignore_index=True)
    df_user_info_2.drop_duplicates(subset=["distributor_id"],
                                   keep='first',
                                   inplace=True)

    # 4. 根据 distributor_id 模糊搜索
    df_sub = df_user_info_2[df_user_info_2['distributor_id'].str.contains(keyword)]
    if not df_sub.empty:
        # 5. 将DataFrame转化成列表
        user_info_lists = df_sub.to_dict('records')
        result['flag'] = True
        result['data'] = user_info_lists
    return result
