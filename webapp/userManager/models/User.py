# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     User
# Description:  用户模型
# Author:       'zhouhanlin'
# CreateDate:   2020/02/19
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from sqlalchemy import TIMESTAMP, text, Column, String, Integer

from webapp import db
from webapp.common.security.encryptAndDecrypt import aes_crypt


class User(db.Model):
    """
    组织职位模型
    SQL建表语句：
                drop table if exists `t_user_info`;
                create table `t_user_info` (
                `id` int ( 11 ) not null auto_increment comment '主键',
                `distributor_id` varchar ( 255 ) not null unique comment '用户id',
                `distributor_status` varchar ( 64 ) not null default '正常' comment '经销商状态',
                `password` varchar ( 255 ) not null comment '经销商密码',
                `create_time` datetime DEFAULT CURRENT_TIMESTAMP comment '用户创建时间',
                `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '用户信息修改时间',
                primary key ( `id` )
                ) default charset = utf8 comment '用户信息表';
    """
    __tablename__ = "t_user_info"
    id = Column("id", Integer, primary_key=True, autoincrement=True, comment="主键")
    distributor_id = Column("distributor_id", String(255), nullable=False, unique=True, comment="用户id")
    distributor_status = Column("distributor_status", String(64), server_default="正常", nullable=False, comment="用户状态")
    password = Column("password", String(1024), nullable=True, comment="用户密码")

    # 条目的创建时间
    create_time = Column("create_time",
                         TIMESTAMP(True),
                         server_default=text('CURRENT_TIMESTAMP'),
                         nullable=False,
                         comment="用户创建时间'"
                         )

    # 条目更新的时候，时间自动更新
    update_time = Column("update_time",
                         TIMESTAMP(True),
                         server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
                         nullable=False,
                         comment="用户信息修改时间"
                         )

    # 给表添加注释
    __table_args__ = ({'comment': '用户信息表'})

    def __init__(self, *args, **kwargs):
        distributor_id = kwargs.get("distributor_id")
        distributor_status = kwargs.get("distributor_status")
        password = kwargs.get("password")

        self.distributor_id = distributor_id
        self.password = aes_crypt.encrypt(password)
        self.distributor_status = distributor_status

    def check_password(self, raw_password):
        if aes_crypt.decrypt(self.password) == raw_password:
            return True
        else:
            return False

    # 模型对象转字典
    @staticmethod
    def to_dict(result):
        from collections import Iterable
        # 转换完成后，删除  '_sa_instance_state' 特殊属性
        try:
            if isinstance(result, Iterable):
                tmp = [dict(zip(res.__dict__.keys(), res.__dict__.values())) for res in result]
                for t in tmp:
                    t.pop('_sa_instance_state')
            else:
                tmp = dict(zip(result.__dict__.keys(), result.__dict__.values()))
                tmp.pop('_sa_instance_state')
            return tmp
        except BaseException:
            raise TypeError('Type error of parameter')
