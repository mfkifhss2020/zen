# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     bluePrint.py
# Description:  蓝本入口
# Author:       zhouhanlin
# CreateDate:   2020/04/07
# Copyright ©2011-2020. Hunan xsyxsc e-Commerce Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from flask import Blueprint

from webapp.common.http.restfulApi import RestfulApi
from webapp.userManager.api.userInfoApi import UserInfo
from webapp.userManager.api.userSubOrgApi import UserSubOrg
from webapp.userManager.api.loginCheckApi import LoginCheck
from webapp.userManager.api.logoutCheckApi import LogoutCheck
from webapp.billManager.api.billManagerApi import BillInfoApi
from webapp.userManager.api.updatePasswdApi import UpdatePassword
from webapp.salesRoomManager.api.salesRoomApi import SalesRoomApi
from webapp.bonusTotal.api.bonusSummaryApi import BonusSummaryApi
from webapp.noticeManager.api.realtimePush import RealtimePushApi
from webapp.noticeManager.api.latestNotice import LatestNoticeApi
from webapp.noticeManager.api.unreadNotice import UnreadNoticeApi
from webapp.billManager.api.distributorNameApi import DistributorName
from webapp.noticeManager.api.noticeManagerApi import NoticeManagerApi

blueprint = Blueprint('blueprint', __name__)
api = RestfulApi(blueprint, catch_all_404s=False)

# 添加路由
api.add_resource(SalesRoomApi, '/salesRoomManager/salesRoom', endpoint='salesRoom')
api.add_resource(BillInfoApi, '/billManager/billInfo', endpoint='billInfo')
api.add_resource(BonusSummaryApi, '/bonusTotal/bonusSummary', endpoint='bonusSummary')
api.add_resource(NoticeManagerApi, '/noticeManager/noticeInfo', endpoint='noticeInfo')
api.add_resource(RealtimePushApi, '/noticeManager/realtimePush', endpoint='realtimePush')
api.add_resource(UserSubOrg, '/userManager/userSubOrg', endpoint='userSubOrg')
api.add_resource(DistributorName, '/billManager/distributorName', endpoint='distributorName')

api.add_resource(UserInfo, '/userManager/userInfo', endpoint='userInfo')
api.add_resource(LoginCheck, '/userManager/login', endpoint='login')
api.add_resource(LogoutCheck, '/userManager/logout', endpoint='logout')
api.add_resource(UpdatePassword, '/userManager/updatePassword', endpoint='updatePassword')
api.add_resource(LatestNoticeApi, '/userManager/latestNotice', endpoint='latestNotice')
api.add_resource(UnreadNoticeApi, '/userManager/unreadNotice', endpoint='unreadNotice')
