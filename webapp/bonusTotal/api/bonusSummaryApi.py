# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     bonusSummaryApi.py
# Description:  奖金统计查询API接口
# Author:       'zhouhanlin'
# CreateDate:   2020/02/21
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
from datetime import datetime, date
from flask import current_app, request
from dateutil.relativedelta import relativedelta
from flask_restful import Resource, fields, reqparse, marshal

from webapp.common.http.respBody import RespBody, MsgDesc
from webapp.bonusTotal.models.bonusSummary import BonusSummary
from webapp.common.utils.datetimeFormatter import get_month_range
from webapp.bonusTotal.utils.bonusCalc import allocate_bonus_calc
from webapp.common.http.decorators import login_required, access_auth

query_fields = {
    'distributor_id': fields.String,
    'distributor_name': fields.String,
    'phase': fields.Integer,
    'introduce_bonus': fields.Integer,
    'room_id': fields.String,
    'phase_bonus': fields.Integer,
    'shop_commission': fields.Float,
    'total_bonus': fields.Float,
    'allocate_bonus': fields.Float,
    'mark': fields.String,
    'total_month': fields.String
}


class BonusSummaryApi(Resource):

    # decorators = [check_http_headers, check_request_frequency]
    def __init__(self):
        self.__env = request.environ
        self.remote_ip = self.__env.get("REMOTE_ADDR")
        self.remote_port = self.__env.get("REMOTE_PORT")
        self.protocol = self.__env.get("SERVER_PROTOCOL").split("/")[0].lower()
        self.http_host = self.__env.get("HTTP_HOST")
        self.method = self.__env.get("REQUEST_METHOD")
        self.uri = self.__env.get("PATH_INFO")
        self.access_user = request.remote_user
        self.logger = current_app.logger

        if self.access_user:
            self.logger_formatter = "来自IP：<{}>".format(self.remote_ip) + \
                                    "的端口：[{}]的 {} 用户".format(self.remote_port, self.access_user) + \
                                    "通过 {} ".format(self.method) + \
                                    "方法访问url：{}".format(self.protocol) + "//" + \
                                    "{}".format(self.http_host) + \
                                    "{}".format(self.uri)
        else:
            self.logger_formatter = "来自IP：<{}>".format(self.remote_ip) + \
                                    "的端口：[{}]".format(self.remote_port) + \
                                    "通过 {} ".format(self.method) + \
                                    "方法访问url：{}".format(self.protocol) + "//" + \
                                    "{}".format(self.http_host) + \
                                    "{}".format(self.uri)

    @login_required
    @access_auth
    def get(self):
        """
        查询当前月份奖金，指定月份奖金，根据播出奖金计算的当前月份奖金，指定月份奖金
        :return: json字符串
        """
        # 获取请求参数, bundle_errors: 错误捆绑在一起并立即发送回客户端
        parse = reqparse.RequestParser(bundle_errors=True)

        # location表示获取args中的关键字段进行校验，required表示必填不传报错，type表示字段类型
        parse.add_argument("current_page", type=int, help="当前页数校验错误", required=True, location='args')
        parse.add_argument("per_page", type=int,
                           help="必须设置页码条目数为10，20，50，或者100",
                           choices=[10, 20, 50, 100],
                           required=True,
                           location='args'
                           )

        parse.add_argument("month", type=str, required=False, location='args')
        parse.add_argument("allocate_all_bonus", type=str, required=False, location='args')

        # 获取传输的值/strict=True代表设置如果传以上未指定的参数主动报错
        args = parse.parse_args(strict=True)

        # 获取播出总奖金
        allocate_all_bonus = args.get("allocate_all_bonus")
        # 获取月份参数
        month = args.get("month")
        current_page = args.get("current_page")
        per_page = args.get("per_page")
        if month is not None:
            # 求取当前时间6个月前的时间
            font_6_time = date.today() - relativedelta(months=5)
            month_list = get_month_range(font_6_time, datetime.now())
            if month not in month_list:
                self.logger.error(MsgDesc.h_401_120.value + "<{}>".format(month))
                self.logger.error(self.logger_formatter + " 失败...")
                return RespBody.custom(code=120, string=MsgDesc.h_401_120.value), 401
            else:
                if allocate_all_bonus is None:
                    record = BonusSummary.query.filter_by(total_month=month).order_by(
                        BonusSummary.id.desc()).all()
                    total = len(record)
                    if total > 0:
                        if total % per_page == 0:
                            pages = total // per_page
                        else:
                            pages = total // per_page + 1
                        if current_page > pages:
                            self.logger.error(MsgDesc.h_400_100.value)
                            self.logger.error(self.logger_formatter + " 失败...")
                            return RespBody.custom(string=MsgDesc.h_400_100.value), 400
                        else:
                            offset_start = (current_page - 1) * per_page
                            offset_end = current_page * per_page
                            self.logger.info(self.logger_formatter + " 成功...")
                            return RespBody.custom(result="success",
                                                   code=200,
                                                   string=MsgDesc.h_200_200.value,
                                                   data=marshal(record[offset_start:offset_end], query_fields),
                                                   current_page=current_page,
                                                   per_page=per_page,
                                                   pages=pages,
                                                   total=total), 200
                    else:
                        self.logger.error(MsgDesc.h_404_100.value)
                        self.logger.error(self.logger_formatter + " 失败...")
                        return RespBody.custom(string=MsgDesc.h_404_100.value), 404
                else:
                    result = allocate_bonus_calc(month, allocate_all_bonus, current_page, per_page)
                    if result.get('status') == 400:
                        self.logger.error(MsgDesc.h_400_100.value)
                        self.logger.error(self.logger_formatter + " 失败...")
                        return RespBody.custom(string=MsgDesc.h_400_100.value), 400
                    elif result.get('status') == 404:
                        self.logger.error(MsgDesc.h_404_100.value)
                        self.logger.error(self.logger_formatter + " 失败...")
                        return RespBody.custom(string=MsgDesc.h_404_100.value), 404
                    elif result.get('status') is None:
                        result['data'] = marshal(result.get('data'), query_fields)
                        result['code'] = 200
                        result['message'] = MsgDesc.h_200_200.value
                        self.logger.info(self.logger_formatter + " 成功...")
                        return result, 200
        else:
            if allocate_all_bonus is None:
                month = datetime.now().strftime('%Y-%m')
                data = BonusSummary.query.filter_by(total_month=month).order_by(
                    BonusSummary.id.desc()).all()
                total = len(data)
                if total > 0:
                    if total % per_page == 0:
                        pages = total // per_page
                    else:
                        pages = total // per_page + 1
                    if current_page > pages:
                        self.logger.error(MsgDesc.h_401_100.value)
                        self.logger.error(self.logger_formatter + " 失败...")
                        return RespBody.custom(string=MsgDesc.h_401_100.value), 401
                    else:
                        offset_start = (current_page - 1) * per_page
                        offset_end = current_page * per_page
                        self.logger.info(self.logger_formatter + " 成功...")
                        return RespBody.custom(result="success",
                                               code=200,
                                               string=MsgDesc.h_200_200.value,
                                               data=marshal(data[offset_start:offset_end], query_fields),
                                               current_page=current_page,
                                               per_page=per_page,
                                               pages=pages,
                                               total=total), 200
                else:
                    self.logger.error(MsgDesc.h_404_100.value)
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(string=MsgDesc.h_404_100.value), 404
            else:
                month = datetime.now().strftime('%Y-%m')
                result = allocate_bonus_calc(month, allocate_all_bonus, current_page, per_page)
                if result.get('status') == 400:
                    self.logger.error(MsgDesc.h_400_100.value)
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(string=MsgDesc.h_400_100.value), 400
                elif result.get('status') == 404:
                    self.logger.error(MsgDesc.h_404_100.value)
                    self.logger.error(self.logger_formatter + " 失败...")
                    return RespBody.custom(string=MsgDesc.h_404_100.value), 404
                elif result.get('status') is None:
                    result['data'] = marshal(result.get('data'), query_fields)
                    result['code'] = 200
                    result['message'] = MsgDesc.h_200_200.value
                    self.logger.info(self.logger_formatter + " 成功...")
                    return result, 200
