# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     pandasExtend.py
# Description:  DOTO
# Author:       'zhouhanlin'
# CreateDate:   2020/02/22
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
import pandas as pd


def datetime2string(df_object, src_column_name, dts_column_name, d_type, str_form='%Y-%m-%d'):
    """
    将DataFrame中的列数据类型datetime64[ns]与字符串之间互转
    :param DataFrame df_object: DataFrame对象
    :param str src_column_name: 需要转换的DataFrame列名称
    :param str dts_column_name: 转换后的DataFrame列名称
    :param str d_type: 转换后的类型
    :param str str_form: 转换格式
    :return: DataFrame
    """
    # 判断参数类型是否正确
    if isinstance(df_object, pd.core.frame.DataFrame):
        if d_type == 'str':
            df_object[dts_column_name] = df_object[src_column_name].apply(lambda x: x.strftime(str_form))
            return df_object
        elif d_type == 'datetime':
            # 字符串转datetime64[ns]格式
            df_object[dts_column_name] = pd.to_datetime(df_object['src_column_name'], format=str_form)
            return df_object
        else:
            raise Exception("不支持该类型：{}的转换。".format(d_type))
    else:
        raise Exception("参数格式不正确，非DataFrame对象。")
