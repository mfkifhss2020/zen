# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  webapp
# FileName:     __init__.py
# Description:  环境变量提取
# Author:       zhouhanlin
# CreateDate:   2020/02/19
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
import os
import platform
from flask_sqlalchemy import SQLAlchemy
from flask_apscheduler import APScheduler

# 控制当使用import * 到出模块中的类时，只支持导出[]的是类
# 对from * import * 导出无效
__all__ = ['env', 'db', 'scheduler']


class Environ(object):

    def __init__(self):
        self.project = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

    @property
    def separator(self):
        if "Windows" in platform.system():
            sign = "\\"
        else:
            sign = "/"
        return sign

    @property
    def src(self):
        abs_path = self.project + self.separator + "webapp"
        return abs_path

    @property
    def conf(self):
        abs_path = self.project + self.separator + "conf"
        return abs_path

    @property
    def logs(self):
        abs_path = self.project + self.separator + "logs"
        return abs_path

    @property
    def bin(self):
        abs_path = self.project + self.separator + "bin"
        return abs_path

    @property
    def static(self):
        abs_path = self.project + self.separator + "static"
        return abs_path

    @property
    def templates(self):
        abs_path = self.project + self.separator + "templates"
        return abs_path

    @property
    def migrate(self):
        abs_path = self.project + self.separator + "migrations"
        return abs_path


db = SQLAlchemy()
scheduler = APScheduler()
env = Environ()
