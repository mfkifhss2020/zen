# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     scheduleTask.py
# Description:  定时任务调度
# Author:       'zhouhanlin'
# CreateDate:   2020/02/21
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
import datetime as dt


class SchedulerTask(object):
    """
    任务配置类
    """
    """
    apscheduler.job.Job(scheduler, id=None, **kwargs)
        id(str)  –该作业的唯一标识符
        name(str)–任务描述
        func     –任务执行程序
        args(tuple|list)–# 任务执行程序位置参数
        kwargs(dict)–可调用对象的关键字参数
        coalesce(bool)–是否在多个运行时间到期时仅运行一次作业
        trigger  –控制此作业计划的触发器对象
        executor(str)–将执行此作业的执行者的名称
        misfire_grace_time(int)–允许此作业延迟执行的时间（以秒为单位）
        max_instances(int)–此作业允许的并发执行实例的最大数量
        next_run_time(datetime.datetime)–此作业的下一个计划运行时间
    """
    JOBS = [
        {
            'id': 'task_01',  # 任务id
            'func': 'webapp.bonusTotal.utils.bonusCalc:bonus_audit',
            'args': None,  # 执行程序参数
            'trigger': 'interval',  # 任务执行类型，定时器
            'seconds': 3600,  # 任务执行时间，单位秒
            # 'next_run_time': dt.datetime.now() + dt.timedelta(seconds=12)  # 第一次开始执行时间的
            'next_run_time': dt.datetime.now()  # 定时任务启动，立即执行
        },
        {
            'id': 'task_02',  # 任务id
            'func': 'webapp.userManager.utils.userSupplement:user_audit',
            'args': None,  # 执行程序参数
            'trigger': 'interval',  # 任务执行类型，定时器
            'seconds': 3600,  # 任务执行时间，单位秒
            # 'next_run_time': dt.datetime.now() + dt.timedelta(seconds=12)  # 第一次开始执行时间的
            'next_run_time': dt.datetime.now()  # 定时任务启动，立即执行
        }
    ]

    # 用进程池提升任务处理效率
    # SCHEDULER_EXECUTORS = {
    #     'default': {'type': 'processpool',
    #                 'max_workers': 4
    #                 }
    # }

    SCHEDULER_JOB_DEFAULTS = {
        'coalesce': True,  # 积攒的任务只跑一次
        'max_instances': 1,  # 支持1个实例并发
        'misfire_grace_time': 5  # 5秒的任务超时容错

    }

    SCHEDULER_TIMEZONE = 'Asia/Shanghai'
    SCHEDULER_API_ENABLED = True
