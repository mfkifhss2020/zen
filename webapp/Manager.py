# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     Manager.py
# Description:  数据库维护相关的操作
# Author:       'zhouhanlin'
# CreateDate:   2020/02/19
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
import os
from flask import Flask
from flask_script import Manager
from flask.logging import default_handler
from flask_migrate import MigrateCommand, Migrate

from webapp import db, env
from webapp.userManager.models.User import User
from webapp.billManager.models.billInfo import BillInfo
from webapp.noticeManager.models.noticeInfo import NoticeInfo
from webapp.common.logging import MultiprocessSizeRotatingLog
from webapp.salesRoomManager.models.salesRoom import SalesRoom
from webapp.bonusTotal.models.bonusSummary import BonusSummary
from webapp.common.configuration.DBConfiguretion import DbInitConfig


def create_manager():
    log_param = env.conf + env.separator + "log.properties"
    db_param = env.conf + env.separator + "db.properties"

    logger = MultiprocessSizeRotatingLog(log_param, "manager", env.project, env.logs, separator=env.separator).get_log

    # 初始化APP对象
    app = Flask(__name__)

    # 连接数据库方式
    db_uri = DbInitConfig(db_param, logger, "mariadb").sql_alchemy_conn

    # 初始化日志对象
    app.logger.removeHandler(default_handler)
    app.logger = logger

    # 调式模式切换
    os.environ["FLASK_ENV"] = 'production'
    # os.environ["FLASK_ENV"] = 'development'

    app.config["SQLALCHEMY_DATABASE_URI"] = db_uri
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True

    db.init_app(app)

    manager = Manager(app)

    # 绑定APP和DB, 可指定migrations目录
    migrate = Migrate(app, db, env.migrate)

    # 添加迁移脚本的命令道manager中
    manager.add_command('db', MigrateCommand)

    return manager


application = create_manager()

if __name__ == '__main__':
    application.run()
