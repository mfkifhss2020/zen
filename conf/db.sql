create database zen charset='utf8';  -- 建库
create user 'zenops'@'%' identified by 'zenops';  -- 创建用户， %是占位符，匹配所有，谨慎使用
grant all privileges on zen.* to 'zenops'@'%' identified by 'zenops';  -- 赋予增删改查权限
flush privileges;  -- 刷新配置


# 数据插入
insert into `t_user_info` ( `distributor_id`, `password` ) values ( 'admin','61646d696e' );