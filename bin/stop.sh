#!/bin/bash
user=$USER
param_1="$1"
if [ $user == opsuser ];then
    cmdb_processid_list=`ps -ef|grep service_cmdb.py|grep -v grep|cut -c 9-15`
	if [ ! -n "$param_1" ];then
		if [ ! -n "$cmdb_processid_list" ];then
			echo "cmdb服务没有在运行."
		else
			`ps -ef|grep service_cmdb.py|grep -v grep|cut -c 9-15 |xargs kill -9`
			echo "cmdb服务已经停止..."
		fi
		echo "所有服务停止完成..."
	else
		if [ $param_1 == "cmdb" ];then
			if [ ! -n "$cmdb_processid_list" ];then
				echo -e "cmdb服务没有在运行.\n"
			else
				`ps -ef|grep service_cmdb.py|grep -v grep|cut -c 9-15 |xargs kill -9`
				echo -e "cmdb服务已经停止...\n"
			fi
		else
			echo "输入的参数错误."
			echo -e "例: sh stop.sh [不带参数|cmdb]\n"
		fi
	fi
else
    echo "警告！请使用opsuser用户进行相关操作."
fi	
