#!/bin/bash
param_1="$1"
execute="/opt/cmdb_resource/bin/bin/python"
if [ $USER == "opsuser" ];then
    cmdb_processid_list=`ps -ef|grep service_cmdb.py|grep -v grep|cut -c 9-15`
	`source /opt/cmdb_resource/bin/bin/activate`
	if [ ! -n "$param_1" ];then
		if [ ! -n "$cmdb_processid_list" ];then
			`nohup $execute /opt/cmdb_resource/webapp/service_cmdb.py >/dev/null 2>&1 &`
			echo -e "cmdb服务启动成功."
		else
			echo "cmdb服务已经在运行."
		fi
		echo -e "所有服务启动完成...\n"
	else
		if [ $param_1 == "cmdb" ];then
			if [ ! -n "$cmdb_processid_list" ];then
				`nohup $execute /opt/cmdb_resource/webapp/service_cmdb.py >/dev/null 2>&1 &`
				echo -e "cmdb服务启动成功.\n"
			else
				echo -e "cmdb服务已经在运行...\n"
			fi
		else
			echo "输入的参数错误."
			echo -e "例: sh start.sh [不带参数|cmdb]\n"
		fi
	fi
else
    echo -e "警告！请使用opsuser用户进行相关操作.\n" 
fi
