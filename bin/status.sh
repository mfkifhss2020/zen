#!/bin/bash
user=$USER
if [ $user=="opsuser" ];then
    cmdb_processid_list=`ps -ef|grep service_cmdb.py|grep -v grep|cut -c 9-15`
    if [ -n "$cmdb_processid_list" ];then
        echo "cmdb服务正在运行."
    else
        echo "cmdb服务没有运行."
    fi
else
    echo "警告！请使用opsuser用户进行相关操作."
fi
