"""empty message

Revision ID: cde04074b76d
Revises: aea9847aef03
Create Date: 2020-02-28 17:29:39.393948

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'cde04074b76d'
down_revision = 'aea9847aef03'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('t_user_info',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False, comment='主键'),
    sa.Column('distributor_id', sa.String(length=255), nullable=False, comment='用户id'),
    sa.Column('distributor_name', sa.String(length=255), nullable=False, comment='用户名字'),
    sa.Column('certificate_id', sa.String(length=255), nullable=False, comment='证件号'),
    sa.Column('contact', sa.String(length=255), nullable=False, comment='用户名字'),
    sa.Column('distributor_status', sa.String(length=64), server_default='正常', nullable=False, comment='用户状态'),
    sa.Column('password', sa.String(length=32), nullable=True, comment='联系方式'),
    sa.Column('create_time', sa.TIMESTAMP(timezone=True), server_default=sa.text('CURRENT_TIMESTAMP'), nullable=False, comment="用户创建时间'"),
    sa.Column('update_time', sa.TIMESTAMP(timezone=True), server_default=sa.text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'), nullable=False, comment='用户信息修改时间'),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('distributor_id'),
    comment='用户信息表'
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('t_user_info')
    # ### end Alembic commands ###
