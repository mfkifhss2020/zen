"""empty message

Revision ID: 7be7a429a6c1
Revises: 55beca40d9b2
Create Date: 2020-02-24 10:21:20.683889

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7be7a429a6c1'
down_revision = '55beca40d9b2'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('t_bonus_summary', sa.Column('sub_relation', sa.BLOB(), nullable=False, comment='下级关系'))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('t_bonus_summary', 'sub_relation')
    # ### end Alembic commands ###
