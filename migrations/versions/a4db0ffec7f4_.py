"""empty message

Revision ID: a4db0ffec7f4
Revises: dcfe9e7f1406
Create Date: 2020-03-16 00:58:03.937476

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a4db0ffec7f4'
down_revision = 'dcfe9e7f1406'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('t_notice_info', sa.Column('read_user_id', sa.Text(), nullable=True, comment='已读用户'))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('t_notice_info', 'read_user_id')
    # ### end Alembic commands ###
