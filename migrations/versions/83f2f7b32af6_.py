"""empty message

Revision ID: 83f2f7b32af6
Revises: 4cb900ec12c7
Create Date: 2020-02-21 16:39:51.300255

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '83f2f7b32af6'
down_revision = '4cb900ec12c7'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('t_bonus_summary',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False, comment='主键'),
    sa.Column('distributor_id', sa.String(length=255), nullable=False, comment='经销商ID，值唯一'),
    sa.Column('distributor_name', sa.String(length=255), nullable=False, comment='经销商名称'),
    sa.Column('phase', sa.Integer(), nullable=False, comment='阶段'),
    sa.Column('introduce_bonus', sa.DECIMAL(precision=6, scale=2), nullable=False, comment='介绍奖金'),
    sa.Column('phase_bonus', sa.DECIMAL(precision=6, scale=2), nullable=False, comment='阶段奖金'),
    sa.Column('shop_commission', sa.DECIMAL(precision=6, scale=2), nullable=False, comment='店铺佣金'),
    sa.Column('total_bonus', sa.DECIMAL(precision=12, scale=2), nullable=False, comment='总计奖金'),
    sa.Column('allocate_bonus', sa.DECIMAL(precision=12, scale=2), nullable=True, comment='播出比例奖金'),
    sa.Column('mark', sa.String(length=255), nullable=True, comment='备注'),
    sa.Column('total_month', sa.String(length=10), nullable=False, comment='统计月份'),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('distributor_id'),
    comment='奖金统计信息表'
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('t_bonus_summary')
    # ### end Alembic commands ###
