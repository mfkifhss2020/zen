# -*- coding: utf-8 -*-
"""
# ---------------------------------------------------------------------------------------------------------
# ProjectName:  ZEN
# FileName:     compile.py
# Description:  编译打包。
# Author:       'zhouhanlin'
# CreateDate:   2020/03/04
# Copyright ©2011-2020. Shenzhen iSoftStone Information Technology Company limited. All rights reserved.
# ---------------------------------------------------------------------------------------------------------
"""
import os
import sys
import struct
import zipfile
import platform
import py_compile
import importlib.util
from pathlib import Path
from datetime import datetime

try:
    from concurrent.futures import ProcessPoolExecutor
except ImportError:
    ProcessPoolExecutor = None
from functools import partial


def separator():
    if "Windows" in platform.system():
        sign = "\\"
    else:
        sign = "/"
    return sign


class PathLike(object):

    def __init__(self, path):
        self.path = path

    def __fspath__(self):
        return self.path


def walk_dir(s_dir, d_dir=None, max_levels=100, quiet=0):
    """
    遍历目录
    :param str s_dir: 源文件目录
    :param str d_dir:  目标文件目录
    :param int max_levels: 遍历深度
    :param int quiet: 打印已编译文件的列表: False或0，只输出错误：1， 无输出：2
    :return: list
    """
    if quiet < 2 and isinstance(s_dir, os.PathLike):
        s_dir = os.fspath(s_dir)
    if not quiet:
        print('Listing {!r}...'.format(s_dir))
    try:
        names = os.listdir(s_dir)
    except OSError:
        if quiet < 2:
            print("Can't list {!r}".format(s_dir))
        names = []
    names.sort()
    for name in names:
        if name == '__pycache__':
            continue
        fullname = os.path.join(s_dir, name)
        if d_dir is not None:
            d_file = os.path.join(d_dir, name)
        else:
            d_file = None
        if not os.path.isdir(fullname):
            yield fullname
        elif (max_levels > 0 and name != os.curdir and name != os.pardir and os.path.isdir(
                fullname) and not os.path.islink(fullname)):
            yield from walk_dir(fullname, d_dir=d_file, max_levels=max_levels - 1, quiet=quiet)


def compile_file(s_dir, fullname, d_dir=None, force=False, rx=None, quiet=0, legacy=False, optimize=-1):
    """
    编译成字节文件
    :param str s_dir:              需要编译的python源码根目录
    :param PathLike对象 fullname:  需要编译的文件名，全路径
    :param str d_dir:             指定编译的输出目录
    :param bool force:            如果为True，则强制编译，即使时间戳是最新的
    :param re对象 rx:             正则匹配规则，排除不需要编译的文件，rx = re.compile(r'[/\\][.]svn')
                                 如果文件存在，则不编译该文件并True返回该文件。
    :param int quiet:            打印已编译文件的列表: False或0，只输出错误：1， 无输出：2
    :param bool legacy:          如果为True，则生成文件名.pyc的全路径文件，而不是PEP 3147路径
    :param int optimize:         优化级别：解释器级别 或 -1
    """
    success = True
    if quiet < 2 and isinstance(fullname, os.PathLike):
        fullname = os.fspath(fullname)
    # 获取源文件的名字
    file_name = os.path.basename(fullname)
    file_dir = os.path.dirname(fullname)
    if d_dir is not None:
        if file_dir != s_dir:
            relative_dir = file_dir[len(s_dir):]
            d_dir = d_dir + separator() + relative_dir
        # 拼接目标文件，os.path默认会判断OS平台的类型来添加目录分隔符
        d_file = os.path.join(d_dir, file_name)
    else:
        d_file = None
    if rx is not None:
        mo = rx.search(fullname)
        if mo:
            return success
    if os.path.isfile(fullname):
        if legacy:
            # 指定字节码文件
            if d_file:
                c_file = d_file + 'c'
            else:
                c_file = fullname + 'c'
        else:
            # 缓存目录 __pycache__
            if optimize >= 0:
                opt = optimize if optimize >= 1 else ''
                c_file = importlib.util.cache_from_source(fullname, optimization=opt)
            else:
                c_file = importlib.util.cache_from_source(fullname)
        head, tail = file_name[:-3], file_name[-3:]
        if tail == '.py':
            if not force:
                try:
                    mtime = int(os.stat(fullname).st_mtime)
                    expect = struct.pack('<4sl', importlib.util.MAGIC_NUMBER,
                                         mtime)
                    with open(c_file, 'rb') as handle:
                        actual = handle.read(8)
                    if expect == actual:
                        return success
                except OSError:
                    pass
            if not quiet:
                print('Compiling {!r}...'.format(fullname))
            try:
                ok = py_compile.compile(fullname, c_file, d_file, True,
                                        optimize=optimize)
            except py_compile.PyCompileError as err:
                success = False
                if quiet >= 2:
                    return success
                elif quiet:
                    print('*** Error compiling {!r}...'.format(fullname))
                else:
                    print('*** ', end='')
                # 转义msg中的不可打印字符
                msg = err.msg.encode(sys.stdout.encoding, errors='backslashreplace')
                msg = msg.decode(sys.stdout.encoding)
                print(msg)
            except (SyntaxError, UnicodeError, OSError) as e:
                success = False
                if quiet >= 2:
                    return success
                elif quiet:
                    print('*** Error compiling {!r}...'.format(fullname))
                else:
                    print('*** ', end='')
                print(e.__class__.__name__ + ':', e)
            else:
                if ok == 0:
                    success = False
    return success


def compile_dir(s_dir, max_levels=100, d_dir=None, force=False, rx=None, quiet=0, legacy=False, optimize=-1, workers=1):
    """
    编译目录
    :param PathLike对象 s_dir:       源文件目录
    :param int max_levels:         遍历深度
    :param str d_dir:              目标文件目录
    :param bool force:            如果为True，则强制编译，即使时间戳是最新的
    :param re对象 rx:             正则匹配规则，排除不需要编译的文件，rx = re.compile(r'[/\\][.]svn')
                                 如果文件存在，则不编译该文件并True返回该文件。
    :param int quiet:            打印已编译文件的列表: False或0，只输出错误：1， 无输出：2
    :param bool legacy:          如果为True，则生成文件名.pyc的全路径文件，而不是PEP 3147路径
    :param int optimize:         优化级别：解释器级别 或 -1
    :param workers:              最大并行工作线程数
    :return: bool
    """
    if workers is not None and workers < 0:
        raise ValueError('workers must be greater or equal to 0')

    files = walk_dir(s_dir, quiet=quiet, max_levels=max_levels, d_dir=d_dir)
    success = True
    if workers is not None and workers != 1 and ProcessPoolExecutor is not None:
        workers = workers or None
        with ProcessPoolExecutor(max_workers=workers) as executor:
            # partial 偏函数
            results = executor.map(partial(s_dir,
                                           compile_file,
                                           d_dir=d_dir,
                                           force=force,
                                           rx=rx,
                                           quiet=quiet,
                                           legacy=legacy,
                                           optimize=optimize
                                           ),
                                   files
                                   )
            success = min(results, default=True)
    else:
        for file in files:
            if not compile_file(s_dir, file, d_dir, force, rx, quiet, legacy, optimize):
                success = False
    return success


def compile_path(skip_cur_dir=True, max_levels=100, force=False, quiet=0, legacy=False, optimize=-1):
    """
    编译路径
    :param bool skip_cur_dir:   如果为true，则跳过当前目录（默认为true）
    :param int max_levels:      最大递归级别（默认为0）
    :param bool force:          如果为True，则强制编译，即使时间戳是最新的
    :param int quiet:           打印已编译文件的列表: False或0，只输出错误：1， 无输出：2
    :param bool legacy:         如果为True，则生成文件名.pyc的全路径文件，而不是PEP 3147路径
    :param int optimize:        优化级别：解释器级别 或 -1
    :return: bool
    """
    success = True
    for s_dir in sys.path:
        if (not dir or dir == os.curdir) and skip_cur_dir:
            if quiet < 2:
                print('Skipping current directory')
        else:
            success = success and compile_dir(s_dir=s_dir,
                                              max_levels=max_levels,
                                              force=force,
                                              quiet=quiet,
                                              legacy=legacy,
                                              optimize=optimize
                                              )
    return success


def get_file(input_path, file_list):
    """
    对目录进行深度优先遍历
    :param str input_path: 需要遍历的文件目录
    :param list file_list: 存放文件的列表
    """
    files = os.listdir(input_path)
    for file in files:
        if os.path.isdir(input_path + separator() + file):
            get_file(input_path + separator() + file, file_list)
        else:
            file_list.append(input_path + separator() + file)


def zip_file_path(input_path, zip_file=None):
    """
    压缩文件
    :param str input_path: 压缩的文件夹路径，绝对路径
    :param str zip_file: 压缩包名称，绝对路径
    :return: 无
    """
    current_date = datetime.now().strftime("%Y%m%d_%H%M%S")
    if zip_file is None:
        zip_path = os.path.dirname(input_path)
        zip_name = os.path.basename(zip_path) + "_" + current_date + '.zip'
        project_path = os.path.dirname(os.path.dirname(input_path))
        zip_file = project_path + separator() + zip_name
    f = zipfile.ZipFile(zip_file, 'w', zipfile.ZIP_DEFLATED)
    print("Start zip packaging...")
    for path, dir_names, file_names in os.walk(zip_path):
        # 去掉目标跟路径，只对目标文件夹下边的文件及文件夹进行压缩
        f_path = path.replace(project_path, '')
        for filename in file_names:
            f.write(os.path.join(path, filename), os.path.join(f_path, filename))
    # 调用了close方法才会保证完成压缩
    f.close()
    print("Package completed.")


def main(params_list):
    """
    主函数
    :param list params_list: 参数列表
    :return: 无
    """
    if len(params_list) <= 1:
        print("Please enter Arguments：")
        print("\n\tUsage：python compile.pyc [源码目录<必填>] [字节码目录<必填>] [压缩包目录<选填>]")
    else:
        if Path(params_list[1]).exists():
            if Path(params_list[2]).exists():
                # 编译sys.path中包含的所有目录
                # 通过调用compile_path()可以编译sys.path中包含的所有目录
                # 编译之前重新设置了sys.path的值，这是为了防止compileall去尝试编译系统路径
                # sys.path[:] = [params_list[1]]
                # compile_path(skip_cur_dir=False, max_levels=100, force=True, quiet=0, legacy=True, optimize=-1)
                compile_dir(params_list[1],
                            max_levels=100,
                            d_dir=params_list[2],
                            force=True,
                            legacy=True
                            )
                if len(params_list) == 4:
                    zip_file_path(params_list[2], params_list[3])
                elif len(params_list) == 3:
                    zip_file_path(params_list[2])
            else:
                print("Directory：<" + params_list[2] + ">doesn't exist.")
        else:
            print("Directory：<" + params_list[1] + ">doesn't exist.")


if __name__ == "__main__":
    main(sys.argv)
